#
# spec file for package SFEmultiarch
#
# includes module(s): multiarch
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%define srcname multiarch

Name:					multiarch
Version:                1.0
Source:					http://multiarch.example/%{srcname}-%{version}.tar.gz


%prep
%setup -q -n %{srcname}-%{version}


%build
CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
if test "x$CPUS" = "x" -o $CPUS = 0; then
    CPUS=1
fi

export CC=gcc
export CXX=g++
export F77=gfortran
export FC=gfortran 

# You can redefine the library installation path

%define mylibdir %{_libdir}/mylib

export CFLAGS="%optflags "
export CXXFLAGS="%cxx_optflags "
export FFLAGS="%optflags "
export FCLAGS="%optflags "
export LDFLAGS="%_ldflags "


./configure --prefix=%{_prefix} \
			--bindir=%{_bindir} \
			--libdir=%{mylibdir} \
			--includedir=%{_includedir}/%{name}

make -j $CPUS


%install
make DESTDIR=$RPM_BUILD_ROOT install


%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Mon Nov 19 2012 - My name <maintainer@example.com>
- Intial spec for a bogus package :)
