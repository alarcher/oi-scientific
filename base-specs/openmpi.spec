#
# spec file for package SFEopenmpi
#
# includes module(s): openmpi
#

%include sfe-gcc.inc

%define srcname openmpi

Name:		openmpi-%{_mpi2_compiler}
Version:	1.6.3
Release:	0
# For release
%define srcversion %{version}
Source:		http://www.open-mpi.org/software/ompi/v1.6/downloads/%{srcname}-%{srcversion}.tar.gz
Patch0:		openmpi-01-configure.diff

# To support multiple MPI implementation let us organize the directories in the following way
# There might be a bug preventing globs to work in %files because %{bld_arch} has quotes
%define mpi2name	%{_mpi2_impl}
%define libdir 		%{_mpi2_libdir}
%define bindir 		%{_mpi2_bindir}
%define sysconfdir 	%{_mpi2_sysconfdir}
%define includedir 	%{_mpi2_includedir}
%define datadir		%{_mpi2_datadir}-%{version}
%define docdir 		%{_mpi2_docdir}-%{version}
%define mandir 		%{_mpi2_mandir}
%define htmldir		%{docdir}/www

%prep
%setup -q -n %{srcname}-%{srcversion}
# Need to remove -export-dynamic flags to sun linker
# http://stackoverflow.com/questions/596076/solaris-linker-equivalent-to-the-gnu-ld-export-dynamic-flag
%patch0 -p1


%build
CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
if test "x$CPUS" = "x" -o $CPUS = 0; then
    CPUS=1
fi

# We have to make sure openmpi wrappers pick the compiler used to build mpi librairies
export CFLAGS="%{optflags} "
export CXXFLAGS="%cxx_optflags "
export FFLAGS="%{optflags} "
export FCFLAGS="%{optflags} "
export LDFLAGS="%_ldflags "

%if %{cc_is_gcc}
export CC=%{_gcc_cc}
export CXX=%{_gcc_cxx}
export F77=%{_gcc_f77}
export FC=%{_gcc_fc}
export CFLAGS="$CFLAGS -Wa,--divide "
%endif

./configure --prefix=%{_prefix} \
	--bindir=%{bindir} \
	--libdir=%{libdir} \
	--includedir=%{includedir} \
	--datadir=%{datadir} \
	--docdir=%{docdir} \
	--htmldir=%{htmldir} \
	--mandir=%{mandir} \
	--sysconfdir=%{sysconfdir} \
	--enable-ipv6 \
	--enable-mpi-f77 --enable-mpi-f90 \
	--enable-mpi-profile --enable-mpi-cxx --enable-mpi-cxx-seek \
	--enable-shared --enable-rpath \
	--with-wrapper-cflags="-L%{libdir} -R%{libdir}" \
	--with-wrapper-cxxflags="-L%{libdir} -R%{libdir}" \
	--with-wrapper-libs="-L%{libdir} -R%{libdir}" \
	--with-wrapper-fflags="-L%{libdir} -R%{libdir}" \
	--with-wrapper-fclags="-L%{libdir} -R%{libdir}"

sed -i -e 's/-export-dynamic//g' ompi/mpi/cxx/Makefile

make -j $CPUS


%install
make DESTDIR=$RPM_BUILD_ROOT install

mv $RPM_BUILD_ROOT%{libdir}/pkgconfig \
	$RPM_BUILD_ROOT%{_libdir}

# need to move the documentation to avoid collision with other mpi implementations
for i in 1 3 7;
do
	if test ! -d $RPM_BUILD_ROOT%{mandir}/man${i}%{mpi2name};
	then
		mv -u $RPM_BUILD_ROOT%{mandir}/man${i} \
			$RPM_BUILD_ROOT%{mandir}/man${i}%{mpi2name};
	fi;

	rm -rf $RPM_BUILD_ROOT%{mandir}/man${i};
done;


%clean
rm -rf $RPM_BUILD_ROOT


#------------------------------------------------------------------------------
%changelog
* Mon Nov 19 2012 Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 1.6.3 with support for 32/64 builds
