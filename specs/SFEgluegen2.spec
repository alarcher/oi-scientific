#
# spec file for package SFEgluegen2
#
# includes module(s): gluegen2
#

%include Solaris.inc
%define cc_is_gcc 1

%ifarch amd64 sparcv9
%include arch64.inc
%include java.inc
%use gluegen2_64 = gluegen2.spec
%endif

%include base.inc
%include java.inc
%use gluegen2 = gluegen2.spec

Name:		SFEgluegen2
IPS_package_name: library/java/gluegen2
Summary:	GlueGen is a tool which automatically generates the Java and JNI code necessary to call C libraries.
Group:		Development/Languages/Java
Version:	%{gluegen2.version}
Release:	%{gluegen2.release}
URL:		http://jogamp.org/gluegen/www
License:	BSD 2-clause, BSD 3-clause, BSD 4-clause
SUNW_Copyright: %{gluegen2.name}.copyright
Meta(info.upstream): JOGAMP forum <http://forum.jogamp.org/>
SUNW_BaseDir: %{_basedir}/%{_subdir}
Docdir:	      %{_defaultdocdir}/doc
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc

BuildRequires: SFEapache-ant
BuildRequires: SFEantlr-devel
BuildRequires: SFEantlr
%if %{cc_is_gcc}
BuildRequires: SFEgcc-46
%endif
BuildRequires: SFEp7zip

%description
GlueGen reads as input ANSI C header files and separate configuration files which provide control over many aspects of the glue code generation. GlueGen uses a complete ANSI C parser and an internal representation (IR) capable of representing all C types to represent the APIs for which it generates interfaces. It has the ability to perform significant transformations on the IR before glue code emission. GlueGen is currently powerful enough to bind even low-level APIs such as the Java Native Interface (JNI) and the AWT Native Interface (JAWT) back up to the Java programming language. 


%prep
rm -rf %{builddir}
%ifarch amd64 sparcv9
mkdir -p %{builddir}/%_arch64
%gluegen2_64.prep -d %{builddir}/%_arch64
%endif

mkdir -p %{builddir}/%base_arch
%gluegen2.prep -d %{builddir}/%base_arch


%build
%ifarch amd64 sparcv9
%gluegen2_64.build -d %{builddir}/%_arch64
%endif

%gluegen2.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%gluegen2_64.install -d %{builddir}/%_arch64
%endif

%gluegen2.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr (0755, root, sys) %{gluegen2.jardir}
%{gluegen2.jardir}/%{gluegen2.name}.jar
%{gluegen2.jardir}/%{gluegen2.name}-rt.jar
%{gluegen2.jardir}/%{gluegen2.name}-rt-natives.jar
%dir %attr (0755, root, bin) %{gluegen2.jnilibdir}
%{gluegen2.jnilibdir}/lib%{gluegen2.name}-rt.so
%ifarch amd64 sparcv9
%dir %attr (0755, root, sys) %{gluegen2_64.jardir}
%{gluegen2_64.jardir}/%{gluegen2_64.name}.jar
%{gluegen2_64.jardir}/%{gluegen2_64.name}-rt.jar
%{gluegen2_64.jardir}/%{gluegen2_64.name}-rt-natives.jar
%dir %attr (0755, root, bin) %{gluegen2_64.jnilibdir}
%{gluegen2_64.jnilibdir}/lib%{gluegen2_64.name}-rt.so
%endif
%dir %attr (0755, root, sys) %{gluegen2.datadir}
%{gluegen2.datadir}/artifact.properties
%{gluegen2.datadir}/%{gluegen2.name}-java-src.zip
%{gluegen2.datadir}/%{gluegen2.name}-rt.jnlp
%dir %attr (0755, root, sys) %{gluegen2.datadir}/src
%{gluegen2.datadir}/src/*
%dir %attr (0755, root, sys) %{gluegen2.datadir}/make
%{gluegen2.datadir}/make/gluegen.properties
%{gluegen2.datadir}/make/Manifest*
%{gluegen2.datadir}/make/*.xml
%{gluegen2.datadir}/make/*.cfg
%{gluegen2.datadir}/make/*.java
%{gluegen2.datadir}/make/142-packages/package-list
%{gluegen2.datadir}/make/gluegen-packages/*
%{gluegen2.datadir}/make/resources/*
%{gluegen2.datadir}/make/scripts/*
%{gluegen2.datadir}/make/stub_includes/*
%dir %attr (0755, root, sys) %{gluegen2.datadir}/test
%{gluegen2.datadir}/test/*


#------------------------------------------------------------------------------
%changelog
* Tue Nov 20 2012 Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 2.0-rc11 
