
# MPI2 implementation can be mpich2 or openmpi
%if "%{mpi2_environment}" == "mpich2"
%define _mpi2_impl 			mpich2
%define mpi2_is_mpich2 		1
%define mpi2_is_openmpi		0
%else if "%{mpi2_environment}" == "openmpi"
%define _mpi2_impl 			openmpi
%define mpi2_is_mpich2 		0
%define mpi2_is_openmpi		1
%endif

%if %{mpi2_is_mpich2}
%define _mpi2_impl 			mpich2
%else if %{mpi2_is_openmpi}
%define _mpi2_impl 			openmpi
%endif

%if %{cc_is_gcc}
%define _mpi2_compiler 		gcc
%else
%define _mpi2_compiler 		cc
%endif

%define _mpi2_bundle		%{_mpi2_impl}-%{_mpi2_compiler}

%define _mpi2_prefix		%{_prefix}/lib/%{_mpi2_impl}/%{_mpi2_compiler}
%if %{opt_arch64}
%define _mpi2_prefix		%{_prefix}/lib/%{_arch64}/%{_mpi2_impl}/%{_mpi2_compiler}
%endif
%define _mpi2_libdir		%{_mpi2_prefix}
%define _mpi2_bindir		%{_mpi2_libdir}/bin
%define _mpi2_sysconfdir	%{_mpi2_libdir}/etc
%define _mpi2_includedir	%{_includedir}/%{_mpi2_impl}
%define _mpi2_datadir		%{_datadir}/%{_mpi2_impl}
%define _mpi2_docdir		%{_docdir}/%{_mpi2_impl}
%define _mpi2_mandir		%{_datadir}/man

%define _mpi2_cc			%{_mpi2_bindir}/mpicc
%define _mpi2_cxx			%{_mpi2_bindir}/mpicxx
%define _mpi2_f77			%{_mpi2_bindir}/mpif77
%define _mpi2_f90			%{_mpi2_bindir}/mpif90
%define _mpi2_fc			%{_mpi2_bindir}/mpif90

