#
# spec file for package SFEsuitesparse
#
# includes module(s): suitesparse
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include Solaris.inc
%define cc_is_gcc 0

# SunCC

%ifarch amd64 sparcv9
%include arch64.inc
%use suitesparse_64 = suitesparse.spec
%endif

%include base.inc
%use suitesparse = suitesparse.spec

Name:                   SFEsuitesparse
IPS_Package_Name:	 	library/math/suitesparse
Summary:                Collection of libraries for computations involving sparse matrices
Group:                  Utility
Version:                %{suitesparse.version}
URL:		         	http://www.cise.ufl.edu/research/sparse/suitesparse/
License: 		 		GPLv2 LGPLv2.1
SUNW_Copyright: 		%{suitesparse.name}.copyright
SUNW_BaseDir:           %{_basedir}
BuildRoot:              %{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc


%description
SuiteSparse is a single archive that contains all packages authored or co-authored by Tim Davis for computations involving sparse matrices, including:

    - AMD: symmetric approximate minimum degree
    - BTF: permutation to block triangular form
    - CAMD: symmetric approximate minimum degree
    - CCOLAMD: constrained column approximate minimum degree
    - COLAMD: column approximate minimum degree
    - CHOLMOD: sparse supernodal Cholesky factorization and update/downdate
    - CSparse: a concise sparse matrix package
    - CXSparse: an extended version of CSparse
    - KLU: sparse LU factorization, for circuit simulation
    - LDL: a simple LDL^T factorization
    - UMFPACK: sparse multifrontal LU factorization
    - RBio: MATLAB toolbox for reading/writing sparse matrices
    - UFconfig: common configuration for all but CSparse
    - SuiteSparseQR: multifrontal sparse QR 

NOTE: Built without METIS support.


%prep
rm -rf %{builddir}
%ifarch amd64 sparcv9
mkdir -p %{builddir}/%_arch64
%suitesparse_64.prep -d %{builddir}/%_arch64
%endif

mkdir -p %{builddir}/%base_arch
%suitesparse.prep -d %{builddir}/%base_arch


%build
%ifarch amd64 sparcv9
%suitesparse_64.build -d %{builddir}/%_arch64
%endif

%suitesparse.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%suitesparse_64.install -d %{builddir}/%_arch64
%endif

%suitesparse.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr (0755, root, bin) %{_libdir}/%{suitesparse.name}
%{_libdir}/%{suitesparse.name}/lib*.a
%ifarch amd64 sparcv9
%dir %attr (0755, root, bin) %{_libdir}/%{_arch64}/%{suitesparse_64.name}
%{_libdir}/%{_arch64}/%{suitesparse.name}/lib*.a
%endif
%dir %attr (0755, root, bin) %{_includedir}/%{suitesparse.name}
%{_includedir}/%{suitesparse.name}/*.h
%{_includedir}/%{suitesparse.name}/*.hpp


#------------------------------------------------------------------------------
%changelog
* Wed Nov 21 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 5.6.1.
