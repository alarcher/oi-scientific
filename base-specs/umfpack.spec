#
# spec file for package SFEumfpack
#
# includes module(s): umfpack
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include sfe-gcc.inc

%define srcname UMFPACK

Name:					umfpack
Version:                5.6.1
Source:		         	http://www.cise.ufl.edu/research/sparse/umfpack/%{srcname}-%{version}.tar.gz


%prep
%setup -q -n %{srcname}


%build
CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
if test "x$CPUS" = "x" -o $CPUS = 0; then
    CPUS=1
fi

%if %{cc_is_gcc}
export CC=%{_gcc_prefix}/bin/gcc
export F77=%{_gcc_prefix}/bin/gfortran
export FC=%{_gcc_prefix}/bin/gfortran 
%endif

%define blaslibdir %{_libdir}/atlas
%define blasincludedir %{_includedir}/atlas

export CFLAGS="%optflags -I -L%{blaslibdir} -R%{blaslibdir}"
export FFLAGS="%optflags -L%{blaslibdir} -R%{blaslibdir}"
export FCLAGS="%optflags -L%{blaslibdir} -R%{blaslibdir}"
export LDFLAGS="%_ldflags -L%{blaslibdir} -R%{blaslibdir}"

%if %{opt_arch64}
%define 64bit_flag -DBLAS64
%endif

./configure --prefix=%{_prefix} \
			--bindir=%{_bindir} \
			--libdir=%{_libdir} \
			--includedir=%{_includedir}/%{name} \
			--with-blas=%{blaslibdir}/libf77blas.a \
			--with-lapack=%{lapackdir}/libatlas.a

make -j $CPUS


%install
make DESTDIR=$RPM_BUILD_ROOT install


%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Wed Nov 21 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 5.6.1.
