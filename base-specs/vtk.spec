#
# spec file for package SFEvtk
#
# includes module(s): vtk
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include sfe-gcc.inc

%define srcname 	vtk
%define datasrcname vtkdata

%define relver		5.10

Name:					vtk
Version:                %{relver}.1
Source:					http://www.vtk.org/files/release/%{relver}/%{srcname}-%{version}.tar.gz
Source1:				http://www.vtk.org/files/release/%{relver}/%{datasrcname}-%{version}.tar.gz
%define dirname			VTK%{version}
%define datadirname		VTKData%{version}

%prep
tar xzf %{SOURCE1}
%setup -q -n %{dirname}


%build
CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
if test "x$CPUS" = "x" -o $CPUS = 0; then
    CPUS=1
fi

export CC=gcc
export CXX=g++
export F77=gfortran
export FC=gfortran 

%define blasdir %{_libdir}/atlas
%define lapackdir %{_libdir}/atlas

export CFLAGS="%optflags "
export CXXFLAGS="%cxx_optflags "
export FFLAGS="%optflags "
export FCLAGS="%optflags "
export LDFLAGS="%_ldflags "


./configure --prefix=%{_prefix} \
			--bindir=%{_bindir} \
			--libdir=%{_libdir} \
			--includedir=%{_includedir}/%{name}

make -j $CPUS


%install
make DESTDIR=$RPM_BUILD_ROOT install


%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Fri Nov 23 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Intial spec for version 5.10.1
