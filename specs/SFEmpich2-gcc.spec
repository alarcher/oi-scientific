#
# spec file for package SFEmpich2-gcc
#
# includes module(s): mpich2-gcc
#

%include Solaris.inc
%define cc_is_gcc 1
%define mpi2_environment mpich2

%ifarch amd64 sparcv9
%include arch64.inc
%include mpi.inc
%use mpich2_64 = mpich2.spec
%endif

%include base.inc
%include mpi.inc
%use mpich2 = mpich2.spec

Name:		SFEmpich2-%{_mpi2_compiler}
IPS_package_name: library/mpich2-%{_mpi2_compiler}
Summary:	High-performance and widely portable implementation of the MPI standard
Group:		Development/High Performance Computing
Version:	%{mpich2.version}
Release:	%{mpich2.release}
URL:		http://www.mcs.anl.gov/research/projects/mpich2
License:	MIT
SUNW_Copyright: mpich2.copyright
Meta(info.upstream): MPICH mailing list <mpich-discuss@mcs.anl.gov>
SUNW_BaseDir: %{_basedir}/%{_subdir}
Docdir:	      %{_defaultdocdir}/doc
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc

#BuildRequires:  SFEgcc
Requires:       SFEgccruntime


%description
The goals of MPICH are:
1. to provide an MPI implementation that efficiently supports different computation and communication platforms including commodity clusters (desktop systems, shared-memory systems, multicore architectures), high-speed networks and proprietary high-end computing systems (Blue Gene, Cray)
2. to enable cutting-edge research in MPI through an easy-to-extend modular framework for other derived implementations


%package root
Summary:       %{summary} - Configuration files
SUNW_BaseDir:  /

%package devel
Summary:       %{summary} - Development files
SUNW_BaseDir:  %{_basedir}/%{_subdir}
Requires:      %name


%prep
rm -rf %{builddir}
%ifarch amd64 sparcv9
mkdir -p %{builddir}/%_arch64
%mpich2_64.prep -d %{builddir}/%_arch64
%endif

mkdir -p %{builddir}/%base_arch
%mpich2.prep -d %{builddir}/%base_arch


%build
%ifarch amd64 sparcv9
%mpich2_64.build -d %{builddir}/%_arch64
%endif

%mpich2.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%mpich2_64.install -d %{builddir}/%_arch64
%endif

%mpich2.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr (0755, root, bin) %{mpich2.libdir}
%{mpich2.libdir}/lib*.so*
%{mpich2.libdir}/lib*.a
%{mpich2.libdir}/lib*.la
%dir %attr(0755,root,bin) %{mpich2.bindir}
%{mpich2.bindir}/*
%ifarch amd64 sparcv9
%dir %attr (0755, root, bin) %{mpich2_64.libdir}
%{mpich2_64.libdir}/lib*.so*
%{mpich2_64.libdir}/lib*.a
%{mpich2_64.libdir}/lib*.la
%dir %attr(0755,root,bin) %{mpich2_64.bindir}
%{mpich2_64.bindir}/*
%endif
%dir %attr(0755,root,bin) %{mpich2.datadir}
%{mpich2.datadir}/examples/*
%dir %attr (0755, root, bin) %{mpich2.mandir}
%{mpich2.mandir}/*


%files root
%defattr (-, root, sys)
%dir %attr (0755, root, sys) %{mpich2.sysconfdir}
%{mpich2.sysconfdir}/*.conf
%ifarch amd64 sparcv9
%dir %attr (0755, root, sys) %{mpich2_64.sysconfdir}
%{mpich2_64.sysconfdir}/*.conf
%endif

%files devel
%dir %attr(0755,root,other) %{_docdir}
%dir %attr(0755,root,bin) %{mpich2.docdir}
%{mpich2.docdir}/*.pdf
%dir %attr(0755,root,bin) %{mpich2.htmldir}
%{mpich2.htmldir}/*
%dir %attr (0755, root, bin) %{_includedir}
%dir %attr (0755, root, other) %{mpich2.includedir}
%{mpich2.includedir}/*.h
%{mpich2.includedir}/*.mod
%dir %attr (0755, root, other) %{mpich2.includedir}/primitives
%{mpich2.includedir}/primitives/*.h
%dir %attr (0755, root, other) %{_libdir}/pkgconfig
%{_libdir}/pkgconfig/*.pc
%ifarch amd64 sparcv9
%dir %attr (0755, root, other) %{_libdir}/%{_arch64}/pkgconfig
%{_libdir}/%{_arch64}/pkgconfig/*.pc
%endif


#------------------------------------------------------------------------------
%changelog
* Thu Nov 15 2012 Aurélien Larcher <aurelien.larcher@gmail.com>
- Bump to version 1.5 and added support for 32/64 builds
* Mon Jun 18 2012 Aurélien Larcher <aurelien.larcher@gmail.com>
- Add patch for HYDRA launcher [https://trac.mcs.anl.gov/projects/mpich2/changeset/9011/mpich2/trunk/src/pm/hydra/utils/launch/launch.c]
* Sun Dec 25 2011 Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for mpich2-1.4.1p1
