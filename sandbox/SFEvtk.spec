#
# spec file for package SFEvtk
#
# includes module(s): vtk
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include Solaris.inc

# We need to use gcc as Qt is compiled with g++
%define cc_is_gcc 1
%define mpi2_environment mpich2
%include usr-g++.inc

%ifarch amd64 sparcv9
%include arch64.inc
%include mpi.inc
%use vtk_64 = vtk.spec
%endif

%include base.inc
%include mpi.inc
%use vtk = vtk.spec

Name:					SFEvtk
IPS_Package_Name:		library/vtk
Summary:				The Visualization Toolkit - A high level 3D visualization library
Group:					Utility
Version:				%{vtk.version}
URL:					http://vtk.org
License:				BSD 3-Clause
SUNW_Copyright:			%{vtk.name}.copyright
Meta(info.upstream): 	VTK user list <vtkusers@vtk.org>
SUNW_BaseDir:			%{_basedir}
BuildRoot:				%{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc

%if %{cc_is_gcc}
BuildRequires:	SFEgcc-46
Requires:  		SFEgcc-runtime
BuildRequires:	SFEmpich2-gcc
%endif
# CMake 2.6.3 minimum
BuildRequires:  SFEcmake
BuildRequires:  SFEhdf5

%description
The Visualization Toolkit (VTK) is an open source, freely available software system for 3D computer graphics, modeling, image processing, volume rendering, scientific visualization and information visualization. VTK also includes ancillary support for 3D interaction widgets, two and three-dimensional annotation, and parallel computing. At its core VTK  is implemented as a C++ toolkit, requiring users to build applications by combining various objects into an application. The system also supports automated wrapping of the C++ core into Python, Java and Tcl, so that VTK applications may also be written using these interpreted programming languages.


%prep
rm -rf %{builddir}
%ifarch amd64 sparcv9
mkdir -p %{builddir}/%_arch64
%vtk_64.prep -d %{builddir}/%_arch64
%endif

mkdir -p %{builddir}/%base_arch
%vtk.prep -d %{builddir}/%base_arch


%build
%ifarch amd64 sparcv9
%vtk_64.build -d %{builddir}/%_arch64
%endif

%vtk.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%vtk_64.install -d %{builddir}/%_arch64
%endif

%vtk.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr (0755, root, bin) %{_bindir}
%{_bindir}/%{name}
%dir %attr (0755, root, bin) %{_libdir}
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/*.so*
%dir %attr (0755, root, other) %{_libdir}/pkgconfig
%{_libdir}/pkgconfig/%{name}.pc
%ifarch amd64 sparcv9
%dir %attr (0755, root, bin) %{_bindir}/%{_arch64}
%{_bindir}/%{_arch64}/%{name}
%dir %attr (0755, root, bin) %{_libdir}/%{_arch64}
%{_libdir}/%{_arch64}/*.a
%{_libdir}/%{_arch64}/*.la
%{_libdir}/%{_arch64}/*.so*
%dir %attr (0755, root, other) %{_libdir}/%{_arch64}/pkgconfig
%{_libdir}/%{_arch64}/pkgconfig/%{name}.pc
%endif


#------------------------------------------------------------------------------
%changelog
* Fri Nov 23 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Intial spec for version 5.10.1
