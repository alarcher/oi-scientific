#
# spec file for package SFEarpack-ng
#
# includes module(s): arpack-ng
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include Solaris.inc
%define cc_is_gcc 1

# SunCC

%ifarch amd64 sparcv9
%include arch64.inc
%use arpack_ng_64 = arpack-ng.spec
%endif

%include base.inc
%use arpack_ng = arpack-ng.spec

Name:                   SFEarpack-ng
IPS_Package_Name:	 	library/math/arpack-ng
Summary:                Collection of Fortran77 subroutines designed to solve large scale eigenvalue problems
Group:                  Utility
Version:                %{arpack_ng.version}
URL:		         	http://http://forge.scilab.org/index.php/p/arpack-ng/
License: 		 		BSD3c
SUNW_Copyright: 		%{arpack_ng.name}.copyright
SUNW_BaseDir:           %{_basedir}
BuildRoot:              %{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}_%{version}
%include default-depend.inc

BuildRequires:  SFEatlas


%description
ARPACK is a collection of Fortran77 subroutines designed to solve large scale eigenvalue problems.
Important Features:
- Reverse Communication Interface.
- Single and Double Precision Real Arithmetic Versions for Symmetric, Non-symmetric, Standard or Generalized Problems.
- Single and Double Precision Complex Arithmetic Versions for Standard or Generalized Problems.
- Routines for Banded Matrices - Standard or Generalized Problems.
- Routines for The Singular Value Decomposition.
- Example driver routines that may be used as templates to implement numerous Shift-Invert strategies for all problem types, data types and precision.
- It is free software. arpack-ng (like arpack) is released under the BSD 3 clauses license.
This project is a joint project between Debian, Octave and Scilab in order to provide a common and maintained version of arpack.


%prep
rm -rf %{builddir}
%ifarch amd64 sparcv9
mkdir -p %{builddir}/%_arch64
%arpack_ng_64.prep -d %{builddir}/%_arch64
%endif

mkdir -p %{builddir}/%base_arch
%arpack_ng.prep -d %{builddir}/%base_arch


%build
%ifarch amd64 sparcv9
%arpack_ng_64.build -d %{builddir}/%_arch64
%endif

%arpack_ng.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%arpack_ng_64.install -d %{builddir}/%_arch64
%endif

%arpack_ng.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr (0755, root, bin) %{_bindir}
%{_bindir}/dnsimp
%dir %attr (0755, root, bin) %{_libdir}
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/*.so*
%dir %attr (0755, root, other) %{_libdir}/pkgconfig
%{_libdir}/pkgconfig/arpack.pc
%ifarch amd64 sparcv9
%dir %attr (0755, root, bin) %{_bindir}/%{_arch64}
%{_bindir}/%{_arch64}/dnsimp
%dir %attr (0755, root, bin) %{_libdir}/%{_arch64}
%{_libdir}/%{_arch64}/*.a
%{_libdir}/%{_arch64}/*.la
%{_libdir}/%{_arch64}/*.so*
%dir %attr (0755, root, other) %{_libdir}/%{_arch64}/pkgconfig
%{_libdir}/%{_arch64}/pkgconfig/arpack.pc
%endif


#------------------------------------------------------------------------------
%changelog
* Mon Nov 19 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec with serial support only.
