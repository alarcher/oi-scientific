#
# spec file for package SFEmatio
#
# includes module(s): matio
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%define srcname matio
%define hdf5dirname 

Name:					matio
Version:                1.5.0
Source:		         	http://sourceforge.net/projects/%{name}/files/%{name}/%{version}/%{srcname}-%{version}.tar.gz
Patch0:					matio-01-getopt-getopt.h.diff
Patch1:					matio-02-getopt-getopt_long.c.diff
Patch2:					matio-03-src-mat.c.diff

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p1

%build
CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
if test "x$CPUS" = "x" -o $CPUS = 0; then
    CPUS=1
fi

#export CC=%{_gcc_prefix}/bin/gcc
export CFLAGS="%optflags -L%{_libdir} -R%{_libdir} -I/usr/include/hdf5 -D__EXTENSIONS__"

./configure --prefix=%{_prefix} \
			--bindir=%{_bindir} \
			--libdir=%{_libdir} \
			--includedir=%{_includedir}/%{name} \
			--enable-mat73=yes \
			--with-hdf5=%{_prefix}

make -j $CPUS


%install
make DESTDIR=$RPM_BUILD_ROOT install

rm -rf $RPM_BUILD_ROOT%{_datadir}/info

%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Mon Nov 19 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 1.5.0.
