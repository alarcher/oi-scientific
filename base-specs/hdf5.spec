#
# spec file for package SFEhdf5
#
# includes module(s): hdf5
#
%include sfe-gcc.inc

%define srcname hdf5

Name:		hdf5
Version:	1.8.10
Source:		http://www.hdfgroup.org/ftp/HDF5/current/src/%{srcname}-%{version}.tar.gz
Patch1:		hdf5-01-config-solaris2.x.diff


%prep
%setup -q -n %{name}-%{version}
%patch1 -p1


%build
CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
if test "x$CPUS" = "x" -o $CPUS = 0; then
    CPUS=1
fi

export CC=%{_gcc_prefix}/bin/gcc
export CXX=%{_gcc_prefix}/bin/g++
export F77=%{_gcc_prefix}/bin/gfortran
export FC=%{_gcc_prefix}/bin/gfortran 

export CFLAGS="%{optflags} -D__EXTENSIONS__"
export CXXFLAGS="%cxx_optflags"
export FFLAGS="%{optflags}"
export FCFLAGS="%{optflags}"
export LDFLAGS="%_ldflags"

./configure --prefix=%{_prefix} \
    --bindir=%{_bindir} \
	--datadir=%{_datadir}/%{name}-%{version} \
	--docdir=%{_docdir}/%{name}-%{version} \
	--htmldir=%{_docdir}/%{name}-%{version}/www \
	--includedir=%{_includedir}/%{name} \
	--libdir=%{_libdir} \
	--sysconfdir=%{_sysconfdir}/%{name} \
	--enable-fortran --enable-cxx

make -j$CPUS


%install
make DESTDIR=$RPM_BUILD_ROOT install

if test ! -d $RPM_BUILD_ROOT%{_datadir}/%{name}-%{version};
then
	mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}-%{version};
	mv -u $RPM_BUILD_ROOT%{_datadir}/hdf5_examples \
		$RPM_BUILD_ROOT%{_datadir}/%{name}-%{version}/examples;
fi;
rm -rf $RPM_BUILD_ROOT%{_datadir}/hdf5_examples


%clean
rm -rf $RPM_BUILD_ROOT

#------------------------------------------------------------------------------
%changelog
* Thu Nov 15 2012 Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial version 1.8.10 with only serial support for 32/64 builds
