#
# spec file for package SFEhypre
#
# includes module(s): hypre
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include sfe-gcc.inc

%define srcname hypre
%define basename hypre


Name:					hypre-%{_mpi2_bundle}
Version:                2.9.0
Source:		         	http://computation.llnl.gov/casc/%{srcname}/download/%{srcname}-%{version}b.tar.gz
Patch0:					hypre-01-src-lib-Makefile.diff
%define dirname			%{srcname}-%{version}b
%define srcarchivename	%{srcname}-%{version}b

# MPI dependent
%define libdir 			%{_mpi2_libdir}
%define bindir 			%{_mpi2_bindir}
%define sysconfdir 		%{_mpi2_sysconfdir}

# MPI independent
%define includedir 		%{_includedir}/%{basename}
%define datadir 		%{_datadir}/%{dirname}
%define docdir 			%{_docdir}/%{dirname}
%define htmldir			%{_docdir}/%{dirname}/www
%define mandir			%{_mpi2_mandir}

%prep
%setup -q -n %{srcarchivename}
%patch0 -p0


%build
CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
if test "x$CPUS" = "x" -o $CPUS = 0; then
    CPUS=1
fi

export CC=%{_mpi2_cc}
export CXX=%{_mpi2_cxx}
export F77=%{_mpi2_f77}

%define atlaslibdir %{_libdir}/atlas
%define atlasincludedir %{_includedir}/atlas

export CFLAGS="%optflags -I%{atlasincludedir} -L%{atlaslibdir} -R%{atlaslibdir}"
export CXXFLAGS="%cxx_optflags -I%{atlasincludedir} -L%{atlaslibdir} -R%{atlaslibdir}"
export FFLAGS="%optflags -L%{atlaslibdir} -R%{atlaslibdir}"
export LDFLAGS="%_ldflags -L%{atlaslibdir} -R%{atlaslibdir}"

pushd src

./configure --prefix=%{_prefix} \
			--bindir=%{bindir} \
  			--sysconfdir=%{sysconfdir} \
			--libdir=%{libdir} \
			--includedir=%{includedir} \
			--datadir=%{datadir} \
			--mandir=%{mandir} \
			--docdir=%{docdir} \
			--htmldir=%{htmldir} \
			--enable-shared \
			--enable-fortran \
			--disable-python \
			--disable-java \
			--with-extra-ldpath="%{_mpi2_libdir} %{atlaslibdir}" \
			--with-MPI-include=%{_mpi2_includedir} \
        	--with-timing \
			--with-blas-libs="cblas f77blas atlas" \
			--with-blas-lib-dirs=%{atlaslibdir} \
			--with-lapack-libs="lapack atlas" \
			--with-lapack-lib-dirs=%{atlaslibdir} \
			--with-fei \
			--with-superlu \
			--with-MPI \
			--with-blas \
			--with-lapack

# MPICH2 adds them in the wrapper
#			--with-MPI-libs= \
#			--with-MPI-lib-dirs=%{_mpi2_libdir} \

sed -i 's|^\(HYPRE_INSTALL_DIR\).*|\1 = ${RPM_BUILD_ROOT}%{_prefix}|' \
        config/Makefile.config
sed -i 's|^\(HYPRE_LIB_INSTALL\).*|\1 = ${RPM_BUILD_ROOT}%{libdir}|' \
        config/Makefile.config
sed -i 's|^\(HYPRE_INC_INSTALL\).*|\1 = ${RPM_BUILD_ROOT}%{includedir}|' \
        config/Makefile.config

mkdir -p hypre/lib
make -j $CPUS DESTDIR=$RPM_BUILD_ROOT

popd


%install

pushd src

make DESTDIR=$RPM_BUILD_ROOT install

popd

if test ! -d $RPM_BUILD_ROOT%{docdir};
then
	mkdir -p $RPM_BUILD_ROOT%{docdir};
	for f in HYPRE_ref_manual.pdf HYPRE_usr_manual.pdf bHYPRE_ref_manual.pdf;
	do
		install -m 644 docs/$f $RPM_BUILD_ROOT%{docdir};
	done;
fi;


%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Wed Nov 21 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 2.9.0.
