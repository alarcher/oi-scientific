#
# spec file for package SFEgts
#
# includes module(s): gts
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include sfe-gcc.inc

%define srcname gts

Name:					gts
Version:                121130
%define srcarchivename	%{srcname}-snapshot-%{version}
Source:					http://gts.sourceforge.net/tarballs/%{srcarchivename}.tar.gz

%define bindir
%define libdir			%{_libdir}
%define includedir		%{_includedir}/%{name}

%prep
%setup -q -n %{srcarchivename}


%build
CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
if test "x$CPUS" = "x" -o $CPUS = 0; then
    CPUS=1
fi

export CFLAGS="%optflags "
export CXXFLAGS="%cxx_optflags "
export FFLAGS="%optflags "
export FCLAGS="%optflags "
export LDFLAGS="%_ldflags "


./configure --prefix=%{_prefix} \
			--bindir=%{_bindir} \
			--libdir=%{libdir} \
			--includedir=%{includedir}

make -j $CPUS


%install
make DESTDIR=$RPM_BUILD_ROOT install


%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Mon Dec 24 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Bump to 121130.
* Mon Nov 26 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Intial spec for version 120706.
