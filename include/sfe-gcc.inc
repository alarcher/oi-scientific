%define _gcc_basedir	/usr/gcc
%define _gcc_version	4.6
%define _gcc_prefix		%{_gcc_basedir}/%{_gcc_version}

%define _gcc_bindir		%{_gcc_prefix}/bin
%define _gcc_libdir		%{_gcc_prefix}/lib
%if %{opt_arch64}
%define _gcc_libdir		%{_gcc_prefix}/lib/%{_arch64}
%endif
%define _gcc_datadir	%{_gcc_prefix}/share
%define _gcc_mandir		%{_gcc_prefix}/share/man

%define _gcc_cc			%{_gcc_bindir}/gcc
%define _gcc_cxx		%{_gcc_bindir}/g++
%define _gcc_f77		%{_gcc_bindir}/gfortran
%define _gcc_fc			%{_gcc_bindir}/gfortran
%define _gcc_f90		%{_gcc_bindir}/gfortran
