#
# spec file for package SFEapache-ant
#
# includes module(s): apache-ant
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include Solaris.inc

#%ifarch amd64 sparcv9
#%include arch64.inc
#%use apache_ant_64 = apache-ant.spec
#%endif

%include base.inc
%include java.inc
%use apache_ant = apache-ant.spec

Name:					SFEapache-ant
IPS_Package_Name:		developer/build/ant
Summary:				Java library and command-line tool that help building software.
Group:					Development/Distribution Tools
Version:				1.8.4
URL:					http://ant.apache.org
License:				The Apache Software License
SUNW_Copyright:			%{apache_ant.name}.copyright


SUNW_BaseDir:			%{_basedir}
BuildRoot:				%{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc

#BuildRequires: pkg:/developer/java/junit
Requires:		developer/java/jdk
Requires:		runtime/java
Requires:		consolidation/sfw/sfw-incorporation
Requires:		runtime/perl-510

%description
Apache Ant is a Java library and command-line tool whose mission is to drive processes described in build files as targets and extension points dependent upon each other. The main known usage of Ant is the build of Java applications. Ant supplies a number of built-in tasks allowing to compile, assemble, test and run Java applications. Ant can also be used effectively to build non Java applications, for instance C or C++ applications. More generally, Ant can be used to pilot any type of process which can be described in terms of targets and tasks. 


%package root
Summary:       %{summary} - Configuration files
SUNW_BaseDir:  /


%package devel
Summary:       %{summary} - Configuration files
SUNW_BaseDir:  %{_basedir}/%{_subdir}
Requires:      %name


%prep
rm -rf %{builddir}
#%ifarch amd64 sparcv9
#mkdir -p %{builddir}/%_arch64
#%apache_ant_64.prep -d %{builddir}/%_arch64
#%endif

mkdir -p %{builddir}/%base_arch
%apache_ant.prep -d %{builddir}/%base_arch


%build
#%ifarch amd64 sparcv9
#%apache_ant_64.build -d %{builddir}/%_arch64
#%endif

%apache_ant.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
#%ifarch amd64 sparcv9
#%apache_ant_64.install -d %{builddir}/%_arch64
#%endif

%apache_ant.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr (0755, root, bin) %{_bindir}
%{_bindir}/ant
%{_bindir}/antRun
%{_bindir}/antRun.pl
%{_bindir}/runant.pl
%{_bindir}/complete-ant-cmd.pl
%dir %attr (0755, root, bin) %{apache_ant.libdir}
%{apache_ant.libdir}/ant*.jar
%dir %attr (0755, root, bin) %{apache_ant.sfwbindir}
%{apache_ant.sfwbindir}/ant
%{apache_ant.sfwbindir}/antRun
%{apache_ant.sfwbindir}/antRun.pl
%{apache_ant.sfwbindir}/runant.pl
%{apache_ant.sfwbindir}/complete-ant-cmd.pl
%dir %attr (0755, root, bin) %{apache_ant.sfwlibdir}
%{apache_ant.sfwlibdir}/ant

%files root
%defattr (-, root, sys)
%dir %attr (0755, root, sys) %{apache_ant.sysconfdir}
%{apache_ant.sysconfdir}/*

%files devel
%dir %attr(0755,root,other) %{apache_ant.docdir}
%{apache_ant.docdir}/WHATSNEW
%{apache_ant.docdir}/LICENSE
%{apache_ant.docdir}/KEYS
%{apache_ant.docdir}/README
%{apache_ant.docdir}/manual/*
%dir %attr(0755,root,other) %{apache_ant.javadocdir}
%{apache_ant.javadocdir}/*


#------------------------------------------------------------------------------
%changelog
* Thu Nov 22 2012 Aurélien Larcher <aurelien.larcher@gmail.com>
- Intial spec for version 1.8.4
