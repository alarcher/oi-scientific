#
# spec file for package SFEapache-ant
#
# includes module(s): apache-ant
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include sfe-gcc.inc

%define srcname apache-ant
%define shortname ant

Name:					apache-ant
Version: 				1.8.4
Source:					http://www.apache.org/dist/ant/source/%{srcname}-%{version}-src.tar.gz
Patch0:					apache-ant-01-build.xml.diff

%define bindir			%{_bindir}
%define libdir			%{_libdir}/%{shortname}
%define docdir			%{_docdir}/%{shortname}
%define mandir			%{_datadir}/%{shortname}
%define javadocdir		%{_docdir}/%{shortname}/javadoc
#%define javadocdir		%{_java_shared_javadocdir}/%{shortname}
%define sysconfdir		%{_sysconfdir}/%{shortname}

# Ensure backwared compatibility
%define sfwbindir		/usr/sfw/bin
%define sfwlibdir		/usr/sfw/lib

%prep
%setup -q -n %{srcname}-%{version}
%patch0 -p0

%build

find . -name "*.jar" -delete
export JAVA_HOME=%{_java_jdk_home}

sh bootstrap.sh

export CLASSPATH=${CLASSPATH}:${JAVA_HOME}/lib/tools.jar

sh build.sh dist

%install

pushd %{srcname}-%{version}

# scripts
mkdir -p $RPM_BUILD_ROOT%{bindir}
mkdir -p $RPM_BUILD_ROOT%{sfwbindir}

pushd bin

install -m 644 ant $RPM_BUILD_ROOT%{bindir}
install -m 644 antRun $RPM_BUILD_ROOT%{bindir}
install -m 644 antRun.pl $RPM_BUILD_ROOT%{bindir}
install -m 644 complete-ant-cmd.pl $RPM_BUILD_ROOT%{bindir}
install -m 644 runant.pl $RPM_BUILD_ROOT%{bindir}

popd

# jars
mkdir -p $RPM_BUILD_ROOT%{libdir}
mkdir -p $RPM_BUILD_ROOT%{sfwlibdir}
mv etc/ant-bootstrap.jar lib
cp -p lib/%{shortname}*.jar $RPM_BUILD_ROOT%{libdir}

# create symlinks to sfw
pushd $RPM_BUILD_ROOT%{sfwbindir}
	ln -sf ../../bin/* .
popd
pushd $RPM_BUILD_ROOT%{sfwlibdir}
	ln -sf ../../share/lib/%{shortname} %{shortname}
popd

# configuration
mkdir -p $RPM_BUILD_ROOT%{sysconfdir}
cp -pr etc/* $RPM_BUILD_ROOT%{sysconfdir}

# documentation
mkdir -p $RPM_BUILD_ROOT%{docdir}
cp -pr manual KEYS README LICENSE WHATSNEW $RPM_BUILD_ROOT%{docdir}

popd

pushd build

mkdir -p $RPM_BUILD_ROOT%{javadocdir}
cp -pr javadocs/* $RPM_BUILD_ROOT%{javadocdir}

popd

%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Thu Nov 22 2012 Aurélien Larcher <aurelien.larcher@gmail.com>
- Intial spec for version 1.8.4
