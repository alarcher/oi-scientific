#
# spec file for package SFEumfpack
#
# includes module(s): umfpack
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include Solaris.inc
%define cc_is_gcc 1

# SunCC

%ifarch amd64 sparcv9
%include arch64.inc
%use umfpack_64 = umfpack.spec
%endif

%include base.inc
%use umfpack = umfpack.spec

Name:                   SFEumfpack
IPS_Package_Name:	 	library/math/umfpack
Summary:                Unsymmetric multifrontal sparse LU factorization package 
Group:                  Utility
Version:                %{umfpack.version}
URL:		         	http://www.cise.ufl.edu/research/sparse/umfpack/
License: 		 		GNU GPLv2
SUNW_Copyright: 		%{umfpack.name}.copyright
SUNW_BaseDir:           %{_basedir}
BuildRoot:              %{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc


%description
UMFPACK is a set of routines for solving unsymmetric sparse linear systems, Ax=b, using the Unsymmetric MultiFrontal method. Written in ANSI/ISO C, with a MATLAB (Version 6.0 and later) interface. Appears as a built-in routine (for lu, backslash, and forward slash) in MATLAB. Includes a MATLAB interface, a C-callable interface, and a Fortran-callable interface. Note that "UMFPACK" is pronounced in two syllables, "Umph Pack". It is not "You Em Ef Pack".


%prep
rm -rf %{builddir}
%ifarch amd64 sparcv9
mkdir -p %{builddir}/%_arch64
%umfpack_64.prep -d %{builddir}/%_arch64
%endif

mkdir -p %{builddir}/%base_arch
%umfpack.prep -d %{builddir}/%base_arch


%build
%ifarch amd64 sparcv9
%umfpack_64.build -d %{builddir}/%_arch64
%endif

%umfpack.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%umfpack_64.install -d %{builddir}/%_arch64
%endif

%umfpack.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr (0755, root, bin) %{_bindir}
%{_bindir}/dnsimp
%dir %attr (0755, root, bin) %{_libdir}
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/*.so*
%dir %attr (0755, root, other) %{_libdir}/pkgconfig
%{_libdir}/pkgconfig/arpack.pc
%ifarch amd64 sparcv9
%dir %attr (0755, root, bin) %{_bindir}/%{_arch64}
%{_bindir}/%{_arch64}/dnsimp
%dir %attr (0755, root, bin) %{_libdir}/%{_arch64}
%{_libdir}/%{_arch64}/*.a
%{_libdir}/%{_arch64}/*.la
%{_libdir}/%{_arch64}/*.so*
%dir %attr (0755, root, other) %{_libdir}/%{_arch64}/pkgconfig
%{_libdir}/%{_arch64}/pkgconfig/arpack.pc
%endif


#------------------------------------------------------------------------------
%changelog
* Wed Nov 21 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 5.6.1.
