#
# spec file for package SFEjogl2
#
# includes module(s): jogl2
#

%define srcname jogl

Name:		jogl2
Version:	2.0
Release:	11
# For release
%define srcversion %{version}
%define srcarchivename %{srcname}-v%{version}-rc%{release}
Source0:	http://jogamp.org/deployment/v%{version}-rc%{release}/archive/Sources/%{srcarchivename}.tar.7z

#http://jogamp.org/deployment/v2.0-rc11/archive/Sources/jogl-v2.0-rc11.tar.7z

BuildRequires: SFEantlr-devel
BuildRequires: SFEantlr
BuildRequires: SFEp7zip

%define libdir  %{_libdir}/%{name}
%define datadir  %{_datadir}/%{name}

%prep
# Unpacking 7z is not supported
# We need to copy the archive or the following builds will fail since
# source .tar.7z will not exist anymore.
%define archivename %bld_arch-%{srcarchivename}
echo %bld_arch
echo %archivename
cp %{SOURCE0} %{archivename}.tar.7z
p7zip -d %{archivename}.tar.7z
tar -xf %{archivename}.tar
rm %{archivename}.tar

# Fix permissions since we do not use %setup
cd  %{srcarchivename}
chmod -Rf a+rX,u+w,g-w,o-w .

# Requires formally ant 1.8.0 but 1.7.0 seems OK
sed -i -e 's/1.8.0/1.7.0/g'  make/build-common.xml

%build
%define archflag isSolaris32Bit
%ifarch amd64
%define archflag isSolarisAMD64
%endif
export JAVA_HOME=/usr/java
cd %{srcarchivename}

pushd make
ant -D%{archflag}=1
popd

%install
cd %{srcarchivename}
mkdir -p $RPM_BUILD_ROOT%{_libdir}/%{name}
# jars
cp build/jogl.jar $RPM_BUILD_ROOT%{_libdir}/%{name}
cp build/jogl-rt.jar $RPM_BUILD_ROOT%{_libdir}/%{name}
cp build/jogl-rt-natives-solaris-*.jar $RPM_BUILD_ROOT%{_libdir}/%{name}
# jni lib
cp build/obj/lib*.so* $RPM_BUILD_ROOT%{_libdir}/%{name}
if test ! -d $RPM_BUILD_ROOT%{_datadir}/%{name};
then 
	mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}
	cp build/artifact.properties $RPM_BUILD_ROOT%{_datadir}/%{name} 
	cp build/jogl-java-src.zip $RPM_BUILD_ROOT%{_datadir}/%{name} 
	cp build/jogl-rt.jnlp $RPM_BUILD_ROOT%{_datadir}/%{name} 
	# generated source
	cp -r build/gensrc $RPM_BUILD_ROOT%{_datadir}/%{name}/src 
fi;

%clean
rm -rf $RPM_BUILD_ROOT

#------------------------------------------------------------------------------
%changelog
* Tue Nov 21 2012 Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 2.0-rc11 
