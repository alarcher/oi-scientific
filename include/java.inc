
%define _java_jre_home			/usr/java
%define _java_jdk_home			/usr/jdk/latest

%define _java_javac				%{_java_jdk_home}/bin/java
%define _java_javac				%{_java_jdk_home}/bin/javac
%define _java_shared_javadir 	/usr/share/lib/java
%define _java_shared_jnilibdir	/usr/lib/jni
%if %{opt_arch64}
%define _java_java				%{_java_jdk_home}/bin/%{_arch64}/java
%define _java_javac				%{_java_jdk_home}/bin/%{_arch64}/javac
%define _java_shared_javadir 	/usr/share/lib/java/%{_arch64}
%define _java_shared_jnilibdir	/usr/lib/%{_arch64}/jni
%endif

%define _java_shared_javadocdir	/usr/share/lib/java/javadoc




