#
# spec file for package SFEparmetis
#
# includes module(s): parmetis
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include Solaris.inc
%define cc_is_gcc 1
%define mpi2_environment mpich2

%ifarch amd64 sparcv9
%include arch64.inc
%include mpi.inc
%use parmetis_64 = parmetis.spec
%endif

%include base.inc
%include mpi.inc
%use parmetis = parmetis.spec

Name:                   SFEparmetis-%{_mpi2_bundle}
IPS_Package_Name:	 	library/math/%{_mpi2_impl}/%{_mpi2_compiler}/parmetis
Summary:                Parallel Graph Partitioning and Fill-reducing Matrix Ordering
Group:                  Utility
Version:                %{parmetis.version}
URL:		         	http://glaros.dtc.umn.edu/
License: 		 		GNU GPLv2
SUNW_Copyright: 		parmetis.copyright
SUNW_BaseDir:           %{_basedir}
BuildRoot:              %{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc

BuildRequires: SFEcmake
BuildRequires: SFE%{_mpi2_impl}-%{_mpi2_compiler}


%description
ParMETIS is an MPI-based parallel library that implements a variety of algorithms for partitioning unstructured graphs, meshes, and for computing fill-reducing orderings of sparse matrices. ParMETIS extends the functionality provided by METIS and includes routines that are especially suited for parallel AMR computations and large scale numerical simulations. The algorithms implemented in ParMETIS are based on the parallel multilevel k-way graph-partitioning, adaptive repartitioning, and parallel multi-constrained partitioning schemes developed in our lab. 


%prep
rm -rf %{builddir}
%ifarch amd64 sparcv9
mkdir -p %{builddir}/%_arch64
%parmetis_64.prep -d %{builddir}/%_arch64
%endif

mkdir -p %{builddir}/%base_arch
%parmetis.prep -d %{builddir}/%base_arch


%build
%ifarch amd64 sparcv9
%parmetis_64.build -d %{builddir}/%_arch64
%endif

%parmetis.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%parmetis_64.install -d %{builddir}/%_arch64
%endif

%parmetis.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr (0755, root, bin) %{parmetis._mpi2_bindir}
%{parmetis._mpi2_bindir}/mtest
%{parmetis._mpi2_bindir}/parmetis
%{parmetis._mpi2_bindir}/pometis
%{parmetis._mpi2_bindir}/ptest
%dir %attr (0755, root, bin) %{parmetis._mpi2_libdir}
%{parmetis._mpi2_libdir}/libparmetis.a
%ifarch amd64 sparcv9
%dir %attr (0755, root, bin) %{parmetis_64._mpi2_bindir}
%{parmetis_64._mpi2_bindir}/mtest
%{parmetis_64._mpi2_bindir}/parmetis
%{parmetis_64._mpi2_bindir}/pometis
%{parmetis_64._mpi2_bindir}/ptest
%dir %attr (0755, root, bin) %{parmetis_64._mpi2_libdir}
%{parmetis_64._mpi2_libdir}/libparmetis.a
%endif
%dir %attr (0755, root, bin) %{_includedir}
%{_includedir}/parmetis.h


#------------------------------------------------------------------------------
%changelog
* Wed Nov 21 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 4.0.2 with no shared lib.
