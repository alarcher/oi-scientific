#
# spec file for package SFEparmetis
#
# includes module(s): parmetis
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%define srcname metis

Name:					metis4
Version:                4.0.3
Source:		         	http://glaros.dtc.umn.edu/gkhome/fetch/sw/%{srcname}/OLD/%{srcname}-%{version}.tar.gz

%define bindir	 		%{_bindir}
%define includedir 		%{_includedir}/%{name}
%define libdir 			%{_libdir}/%{name}
%define docdir 			%{_docdir}/%{name}-%{version}

%prep
%setup -q -n %{srcname}-%{version}


%build
CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
if test "x$CPUS" = "x" -o $CPUS = 0; then
    CPUS=1
fi

#
sed -i -e 's|OPTFLAGS = -O2|OPTFLAGS = %{optflags}|' Makefile.in
sed -i -e 's|LDOPTIONS = |LDOPTIONS = %{optflags} %{_ldflags}|' Makefile.in

make -j $CPUS

# run tests
pushd Graphs 
	./mtest 4elt.graph
popd

%install

install -m 755 -d $RPM_BUILD_ROOT%{_bindir}
install -m 755 -t $RPM_BUILD_ROOT%{_bindir} graphchk 
install -m 755 -t $RPM_BUILD_ROOT%{_bindir} kmetis
install -m 755 -t $RPM_BUILD_ROOT%{_bindir} mesh2dual
install -m 755 -t $RPM_BUILD_ROOT%{_bindir} mesh2nodal
install -m 755 -t $RPM_BUILD_ROOT%{_bindir} oemetis
install -m 755 -t $RPM_BUILD_ROOT%{_bindir} onmetis
install -m 755 -t $RPM_BUILD_ROOT%{_bindir} partdmesh
install -m 755 -t $RPM_BUILD_ROOT%{_bindir} partnmesh
install -m 755 -t $RPM_BUILD_ROOT%{_bindir} pmetis
install -m 755 -d $RPM_BUILD_ROOT%{includedir}
install -m 644 -t $RPM_BUILD_ROOT%{includedir} Lib/*.h
install -m 755 -d $RPM_BUILD_ROOT%{docdir}
install -m 644 -t $RPM_BUILD_ROOT%{docdir} Doc/manual.ps
install -m 755 -d $RPM_BUILD_ROOT%{libdir}
install -m 644 -t $RPM_BUILD_ROOT%{libdir} libmetis.a


%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Sun Nov 25 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 4.0.3 with no shared lib.
