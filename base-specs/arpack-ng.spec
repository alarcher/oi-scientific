#
# spec file for package SFEarpack-ng
#
# includes module(s): arpack-ng
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include sfe-gcc.inc

%define srcname arpack-ng

Name:					arpack-ng
Version:                3.1.2
Source:		         	http://forge.scilab.org/index.php/p/arpack-ng/downloads/get/%{srcname}_%{version}.tar.gz


%prep
%setup -q -n %{srcname}_%{version}


%build
CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
if test "x$CPUS" = "x" -o $CPUS = 0; then
    CPUS=1
fi

export CC=%{_gcc_prefix}/bin/gcc
export F77=%{_gcc_prefix}/bin/gfortran
export FC=%{_gcc_prefix}/bin/gfortran 

%define blasdir %{_libdir}/atlas
%define lapackdir %{_libdir}/atlas

export CFLAGS="%optflags -L%{blasdir} -R%{blasdir} -L%{lapackdir} -R%{lapackdir}"
export FFLAGS="%optflags -L%{blasdir} -R%{blasdir} -L%{lapackdir} -R%{lapackdir}"
export FCLAGS="%optflags -L%{blasdir} -R%{blasdir} -L%{lapackdir} -R%{lapackdir}"
export LDFLAGS="%_ldflags -L%{blasdir} -R%{blasdir} -L%{lapackdir} -R%{lapackdir}"


./configure --prefix=%{_prefix} \
			--bindir=%{_bindir} \
			--libdir=%{_libdir} \
			--includedir=%{_includedir}/%{name} \
			--with-blas=%{blasdir}/libf77blas.a \
			--with-lapack=%{lapackdir}/libatlas.a

make -j $CPUS


%install
make DESTDIR=$RPM_BUILD_ROOT install


%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Mon Nov 19 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec with serial support only.
