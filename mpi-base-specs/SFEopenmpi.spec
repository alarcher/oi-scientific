#
# spec file for package SFEopenmpi
#
# includes module(s): openmpi
#

%include Solaris.inc
%define cc_is_gcc 1

%ifarch amd64 sparcv9
%include arch64.inc
%include mpi.inc
%use openmpi_64 = openmpi.spec
%endif

%include base.inc
%include mpi.inc
%use openmpi = openmpi.spec

Name:		SFEopenmpi-%{_mpi2_compiler}
IPS_package_name: library/openmpi-%{_mpi2_compiler}
Summary:	High-performance and widely portable implementation of the MPI standard
Group:		Development/High Performance Computing
Version:	%{openmpi.version}
Release:	%{openmpi.release}
URL:		http://www.open-mpi.org
License:	BSD 2-Clause
SUNW_Copyright: openmpi.copyright
Meta(info.upstream): OpenMPI user mailing list <users@open-mpi.org>

SUNW_BaseDir: %{_basedir}/%{_subdir}
Docdir:	      %{_defaultdocdir}/doc
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc

#BuildRequires:  SFEgcc
Requires:       SFEgccruntime


%description
The Open MPI Project is an open source MPI-2 implementation that is developed and maintained by a consortium of academic, research, and industry partners. Open MPI is therefore able to combine the expertise, technologies, and resources from all across the High Performance Computing community in order to build the best MPI library available. Open MPI offers advantages for system and software vendors, application developers and computer science researchers.


%package root
Summary:       %{summary} - Configuration files
SUNW_BaseDir:  /

%package devel
Summary:       %{summary} - Development files
SUNW_BaseDir:  %{_basedir}/%{_subdir}
Requires:      %name


%prep
rm -rf %{builddir}
%ifarch amd64 sparcv9
mkdir -p %{builddir}/%_arch64
%openmpi_64.prep -d %{builddir}/%_arch64
%endif

mkdir -p %{builddir}/%base_arch
%openmpi.prep -d %{builddir}/%base_arch


%build
%ifarch amd64 sparcv9
%openmpi_64.build -d %{builddir}/%_arch64
%endif

%openmpi.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%openmpi_64.install -d %{builddir}/%_arch64
%endif

%openmpi.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr (0755, root, bin) %{openmpi.libdir}
%{openmpi.libdir}/lib*.so*
%{openmpi.libdir}/lib*.a
%{openmpi.libdir}/lib*.la
%{openmpi.libdir}/mpi.mod
%dir %attr (0755, root, bin) %{openmpi.libdir}/openmpi
%{openmpi.libdir}/openmpi/*.la
%{openmpi.libdir}/openmpi/*.so
%dir %attr(0755,root,bin) %{openmpi.bindir}
%{openmpi.bindir}/*
%ifarch amd64 sparcv9
%dir %attr (0755, root, bin) %{openmpi_64.libdir}
%{openmpi_64.libdir}/lib*.so*
%{openmpi_64.libdir}/lib*.a
%{openmpi_64.libdir}/lib*.la
%{openmpi_64.libdir}/mpi.mod
%dir %attr (0755, root, bin) %{openmpi_64.libdir}/openmpi
%{openmpi_64.libdir}/openmpi/*.la
%{openmpi_64.libdir}/openmpi/*.so
%dir %attr(0755,root,bin) %{openmpi_64.bindir}
%{openmpi_64.bindir}/*
%endif
%dir %attr (0755, root, sys) %{_datadir}
%dir %attr (0755, root, bin) %{_datadir}/man
%dir %attr (0755, root, bin) %{_datadir}/man/man1openmpi
%{_datadir}/man/man1openmpi/*
%dir %attr (0755, root, bin) %{_datadir}/man/man3openmpi
%{_datadir}/man/man3openmpi/*
%dir %attr (0755, root, bin) %{_datadir}/man/man7openmpi
%{_datadir}/man/man7openmpi/*
%dir %attr(0755,root,bin) %{_datadir}/%{openmpi.name}-%{version}
%{_datadir}/%{openmpi.name}-%{version}/*.txt
%{_datadir}/%{openmpi.name}-%{version}/*.xml
%{_datadir}/%{openmpi.name}-%{version}/*.dtd
%{_datadir}/%{openmpi.name}-%{version}/*.h
%{_datadir}/%{openmpi.name}-%{version}/*.SPEC
%{_datadir}/%{openmpi.name}-%{version}/libtool
%{_datadir}/%{openmpi.name}-%{version}/config.log
%dir %attr(0755,root,bin) %{_datadir}/%{openmpi.name}-%{version}/openmpi
%{_datadir}/%{openmpi.name}-%{version}/openmpi/*.txt
%{_datadir}/%{openmpi.name}-%{version}/openmpi/openmpi-valgrind.supp
%dir %attr(0755,root,bin) %{_datadir}/%{openmpi.name}-%{version}/openmpi/amca-param-sets
%{_datadir}/%{openmpi.name}-%{version}/openmpi/amca-param-sets/example.conf


%files root
%defattr (-, root, sys)
%dir %attr (0755, root, sys) %{openmpi.cnfdir}
%{openmpi.cnfdir}/*.conf
%{openmpi.cnfdir}/vtsetup-config.xml
%{openmpi.cnfdir}/vtsetup-config.dtd
%{openmpi.cnfdir}/openmpi-default-hostfile
%{openmpi.cnfdir}/openmpi-totalview.tcl
%ifarch amd64 sparcv9
%dir %attr (0755, root, sys) %{openmpi_64.cnfdir}
%{openmpi_64.cnfdir}/*.conf
%{openmpi_64.cnfdir}/vtsetup-config.xml
%{openmpi_64.cnfdir}/vtsetup-config.dtd
%{openmpi_64.cnfdir}/openmpi-default-hostfile
%{openmpi_64.cnfdir}/openmpi-totalview.tcl
%endif

%files devel
%dir %attr(0755,root,other) %{_docdir}
%dir %attr(0755,root,bin) %{_docdir}/%{openmpi.name}-%{version}
%{_docdir}/%{openmpi.name}-%{version}/*
%dir %attr (0755, root, bin) %{_includedir}
%dir %attr (0755, root, other) %{_includedir}/%{openmpi.name}
%{_includedir}/%{openmpi.name}/*.h
%dir %attr (0755, root, other) %{_includedir}/%{openmpi.name}/vampirtrace
%{_includedir}/%{openmpi.name}/vampirtrace/*.h
%{_includedir}/%{openmpi.name}/vampirtrace/*.inc
%dir %attr (0755, root, other) %{_includedir}/%{openmpi.name}/openmpi/ompi/mpi/cxx
%{_includedir}/%{openmpi.name}/openmpi/ompi/mpi/cxx/*.h
%dir %attr (0755, root, other) %{_libdir}/pkgconfig
%{_libdir}/pkgconfig/*.pc
%ifarch amd64 sparcv9
%dir %attr (0755, root, other) %{_libdir}/%{_arch64}/pkgconfig
%{_libdir}/%{_arch64}/pkgconfig/*.pc
%endif


#------------------------------------------------------------------------------
%changelog
* Mon Nov 19 2012 Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 1.6.3 with support for 32/64 builds
