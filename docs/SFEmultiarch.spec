#
# spec file for package SFEmultiarch
#
# includes module(s): multiarch
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

# This is an example of a spec file with 32/64bit support with the GCC compiler

%include Solaris.inc
%define cc_is_gcc 1


%ifarch amd64 sparcv9
%include arch64.inc
%use multiarch_64 = multiarch.spec
%endif

%include base.inc
%use multiarch = multiarch.spec

Name:					SFEmultiarch
IPS_Package_Name:		library/multiarch
Summary:				__add_summary__
Group:					Utility
Version:				%{multiarch.version}
URL:					__add_url__
License:				__add_license__
SUNW_Copyright:			%{multiarch.name}.copyright
SUNW_BaseDir:			%{_basedir}
BuildRoot:				%{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc

#BuildRequires:	SFEgcc
Requires:  		SFEgcc-runtime

%description
__add_description__


%prep
rm -rf %{builddir}
%ifarch amd64 sparcv9
mkdir -p %{builddir}/%_arch64
%multiarch_64.prep -d %{builddir}/%_arch64
%endif

mkdir -p %{builddir}/%base_arch
%multiarch.prep -d %{builddir}/%base_arch


%build
%ifarch amd64 sparcv9
%multiarch_64.build -d %{builddir}/%_arch64
%endif

%multiarch.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%multiarch_64.install -d %{builddir}/%_arch64
%endif

%multiarch.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr (0755, root, bin) %{_bindir}
%{_bindir}/%{name}
%dir %attr (0755, root, bin) %{_libdir}
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/*.so*
%dir %attr (0755, root, other) %{_libdir}/pkgconfig
%{_libdir}/pkgconfig/%{name}.pc
%ifarch amd64 sparcv9
%dir %attr (0755, root, bin) %{_bindir}/%{_arch64}
%{_bindir}/%{_arch64}/%{name}
%dir %attr (0755, root, bin) %{_libdir}/%{_arch64}
%{_libdir}/%{_arch64}/*.a
%{_libdir}/%{_arch64}/*.la
%{_libdir}/%{_arch64}/*.so*
%dir %attr (0755, root, other) %{_libdir}/%{_arch64}/pkgconfig
%{_libdir}/%{_arch64}/pkgconfig/%{name}.pc
%endif


#------------------------------------------------------------------------------
%changelog
* Mon Nov 19 2012 - My name <maintainer@example.com>
- Intial spec for a bogus package :)
