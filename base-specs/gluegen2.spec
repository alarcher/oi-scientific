#
# spec file for package SFEgluegen2
#
# includes module(s): gluegen2
#

%include sfe-gcc.inc

%define srcname gluegen

Name:		gluegen2
Version:	2.0
Release:	11
# For release
%define srcversion %{version}
%define srcarchivename %{srcname}-v%{version}-rc%{release}
Source0:	http://jogamp.org/deployment/v%{version}-rc%{release}/archive/Sources/%{srcarchivename}.tar.7z
# Remove git calls
Patch0:		gluegen2-make-build.xml.diff    
# Fix path to system ant and antlr
Patch1:		gluegen2-make-gluegen-properties.xml.diff
# Rename lib
Patch2:		gluegen2-rename-lib-to-gluegen2.diff

%define jardir		%{_java_shared_javadir}
%define jnilibdir  	%{_java_shared_jnilibdir}
%define datadir		%{_datadir}/%{name}

%prep
# Unpacking 7z is not supported
# We need to copy the archive or the following builds will fail since
# source .tar.7z will not exist anymore.
%define archivename %bld_arch-%{srcarchivename}
echo %bld_arch
echo %archivename
cp %{SOURCE0} %{archivename}.tar.7z
p7zip -d %{archivename}.tar.7z
tar -xf %{archivename}.tar
rm %{archivename}.tar

# Fix permissions since we do not use %setup
cd  %{srcarchivename}
chmod -Rf a+rX,u+w,g-w,o-w .

# Requires formally ant 1.8.0 but 1.7.0 seems OK
sed -i -e 's/1.8.0/1.7.0/g'  make/jogamp-env.xml

%patch0 -p0
%patch1 -p0
%patch2 -p0

%build
%define archflag -DisSolaris32Bit=true -DisSolarisX86=true -Dos.and.arch=solaris-i586 -Dos.arch=i386
#%define archflag -Dos.and.arch=solaris-i586
%if %{opt_arch64}
%define archflag -DisSolaris64Bit=true -DisSolarisAMD64=true -Dos.and.arch=solaris-amd64 -Dos.arch=AMD64
#%define archflag -Dos.and.arch=solaris-amd64
%endif

# Can be either SUN CC ...
export CFLAGS="%{optflags} -D__STDC_VERSION__"
export LDFLAGS="%{optflags} "

# Or gcc...
%if %{cc_is_gcc}
export CC=%{_gcc_cc}
export CFLAGS="%{optflags} -D__GNUC__"
export LDFLAGS="%{optflags} -static-libgcc"
%endif

export JAVA=%{_java_java}
export JAVAC=%{_java_javac}
export JAVA_HOME=%{_java_jdk_home}

pushd %{srcarchivename}

ant -f make/build.xml %{archflag} all &> build_all.log
ant -f make/build.xml -Djavadoc.link=%{_java_javadocdir}/java javadoc

# Cannot understand why 64 bit is not compiled... although the flags for ant are ok but -m32 is passed...
%if %{opt_arch64}
pushd build/obj
export CFLAGS="$CFLAGS -D__unix__ -D__X11__ -DNDEBUG -DSOLARIS -D__x86_64__ -I../gensrc/native -I../gensrc/native/Unix -I%{_java_jdk_home}/include -I%{_java_jdk_home}/include/solaris -I../../make/stub_includes/platform"
export LDFLAGS="$LDFLAGS -static-libgcc -shared"
$CC -c $CFLAGS -o JVMUtil.o ../../src/native/common/JVMUtil.c
$CC -c $CFLAGS -o MachineDescriptionRuntime.o ../../src/native/common/MachineDescriptionRuntime.c
$CC -c $CFLAGS -o UnixDynamicLinkerImpl_JNI.o ../../src/native/unix/UnixDynamicLinkerImpl_JNI.c
$CC -c $CFLAGS -o PointerBuffer.o ../../src/native/common/PointerBuffer.c
$CC $LDFLAGS -shared -o libgluegen-rt.so  JVMUtil.o UnixDynamicLinkerImpl_JNI.o PointerBuffer.o MachineDescriptionRuntime.o 
popd

popd
%endif


%install
cd %{srcarchivename}

# jars
mkdir -p $RPM_BUILD_ROOT%{jardir}
install -m 644 build/gluegen.jar $RPM_BUILD_ROOT%{jardir}/%{name}.jar
install -m 644 build/gluegen-rt.jar $RPM_BUILD_ROOT%{jardir}/%{name}-rt.jar
install -m 644 build/gluegen-rt-natives-*.jar $RPM_BUILD_ROOT%{jardir}/%{name}-rt-natives.jar
# jni lib
mkdir -p $RPM_BUILD_ROOT%{jnilibdir}
install -m 755 build/obj/libgluegen-rt.so $RPM_BUILD_ROOT%{jnilibdir}/lib%{name}-rt.so

# data
if test ! -d $RPM_BUILD_ROOT%{datadir};
then 
	mkdir -p $RPM_BUILD_ROOT%{datadir}
	cp build/artifact.properties $RPM_BUILD_ROOT%{datadir}/artifact.properties
	cp build/gluegen-java-src.zip $RPM_BUILD_ROOT%{datadir}/%{name}-java-src.zip
	cp build/gluegen-rt.jnlp $RPM_BUILD_ROOT%{datadir}/%{name}-rt.jnlp
	# generated source
	cp -pr build/gensrc $RPM_BUILD_ROOT%{datadir}/src 
	# make content
	rm -rf make/lib
	cp -pr make $RPM_BUILD_ROOT%{datadir}
	# test content
	mkdir -p $RPM_BUILD_ROOT%{datadir}/test
	cp -pr test/junit $RPM_BUILD_ROOT%{datadir}/test 
fi;

%clean
rm -rf $RPM_BUILD_ROOT

#------------------------------------------------------------------------------
%changelog
* Tue Nov 20 2012 Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 2.0-rc11 
