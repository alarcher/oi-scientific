#
# spec file for package SFEparmetis
#
# includes module(s): parmetis
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include sfe-gcc.inc

%define srcname parmetis

Name:					parmetis-%{_mpi2_bundle}
Version:                4.0.2
Source:		         	http://glaros.dtc.umn.edu/gkhome/fetch/sw/%{srcname}/%{srcname}-%{version}.tar.gz


%prep
%setup -q -n %{srcname}-%{version}


%build
CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
if test "x$CPUS" = "x" -o $CPUS = 0; then
    CPUS=1
fi

export CC=%{_mpi2_cc}
export CXX=%{_mpi2_cxx}

# __EXTENSIONS__ required because of use of getline()
export CFLAGS="%optflags -D__EXTENSIONS__ -L%{_mpi2_libdir} -R%{_mpi2_libdir}"
export CXXFLAGS="%cxx_optflags -L%{_mpi2_libdir} -R%{_mpi2_libdir}"
export LDFLAGS="%_ldflags -L%{_mpi2_libdir} -R%{_mpi2_libdir}"

make config cc=%{_mpi2_cc} cxx=%{_mpi2_cxx} prefix=%{_mpi2_prefix}

make -j $CPUS


%install
make DESTDIR=$RPM_BUILD_ROOT install

# Put parmetis library in the correct mpi implementation subdirectory
mv $RPM_BUILD_ROOT%{_mpi2_libdir}/lib/libparmetis.a $RPM_BUILD_ROOT%{_mpi2_libdir}/libparmetis.a
rmdir $RPM_BUILD_ROOT%{_mpi2_libdir}/lib

# Copy header to system wide include
if test ! -e $RPM_BUILD_ROOT%{_includedir}/parmetis.h;
then
	mkdir -p $RPM_BUILD_ROOT%{_includedir}
	mv $RPM_BUILD_ROOT%{_mpi2_libdir}/include/parmetis.h $RPM_BUILD_ROOT%{_includedir};
fi;
rm -rf $RPM_BUILD_ROOT%{_mpi2_libdir}/include


%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Wed Nov 21 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 4.0.2 with no shared lib.
