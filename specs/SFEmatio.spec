#
# spec file for package SFEmatio
#
# includes module(s): matio
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include Solaris.inc
%define cc_is_gcc 0

# SunCC

%ifarch amd64 sparcv9
%include arch64.inc
%use matio_64 = matio.spec
%endif

%include base.inc
%use matio = matio.spec

Name:                   SFEmatio
IPS_Package_Name:	 	library/math/matio
Summary:                C library for reading and writing Matlab MAT files
Group:                  Utility
Version:                %{matio.version}
URL:		         	http://sourceforge.net/projects/matio/
License: 		 		Public Domain
SUNW_Copyright: 		%{matio.name}.copyright
SUNW_BaseDir:           %{_basedir}
BuildRoot:              %{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc

%if %{cc_is_gcc}
BuildRequires: SFEgcc-46
Requires: SFEgccruntime
%endif
BuildRequires: SFEhdf5


%description
matio is an C library for reading and writing Matlab MAT files.

%package devel
Summary:       %{summary} - development files
SUNW_BaseDir:  %{_basedir}/%{_subdir}
Requires:      %name


%prep
rm -rf %{builddir}
%ifarch amd64 sparcv9
mkdir -p %{builddir}/%_arch64
%matio_64.prep -d %{builddir}/%_arch64
%endif

mkdir -p %{builddir}/%base_arch
%matio.prep -d %{builddir}/%base_arch


%build
%ifarch amd64 sparcv9
%matio_64.build -d %{builddir}/%_arch64
%endif

%matio.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%matio_64.install -d %{builddir}/%_arch64
%endif

%matio.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr(0755,root,bin) %{_bindir}
%{_bindir}/matdump
%dir %attr (0755, root, bin) %{_libdir}
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/*.so*
%dir %attr (0755, root, other) %{_libdir}/pkgconfig
%{_libdir}/pkgconfig/matio.pc
%dir %attr(0755,root,bin) %{_datadir}/man
%{_datadir}/man/*
%ifarch amd64 sparcv9
%dir %attr(0755,root,bin) %{_bindir}/%{_arch64}
%{_bindir}/%{_arch64}/matdump
%dir %attr (0755, root, bin) %{_libdir}/%{_arch64}
%{_libdir}/%{_arch64}/*.a
%{_libdir}/%{_arch64}/*.la
%{_libdir}/%{_arch64}/*.so*
%dir %attr (0755, root, other) %{_libdir}/%{_arch64}/pkgconfig
%{_libdir}/%{_arch64}/pkgconfig/matio.pc
%endif

%files devel
%dir %attr (0755, root, bin) %{_includedir}
%dir %attr (0755, root, other) %{_includedir}/%{matio.name}
%{_includedir}/%{matio.name}/*.h

#------------------------------------------------------------------------------
%changelog
* Mon Nov 19 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 1.5.0.
