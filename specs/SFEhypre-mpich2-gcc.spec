#
# spec file for package SFEhypre
#
# includes module(s): hypre
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include Solaris.inc
%define cc_is_gcc 1
%define mpi2_environment mpich2

%ifarch amd64 sparcv9
%include arch64.inc
%include mpi.inc
%use hypre_64 = hypre.spec
%define bindir64 %{hypre_64._mpi2_bindir}
%define libdir64 %{hypre_64._mpi2_libdir}
%endif

%include base.inc
%include mpi.inc
%use hypre = hypre.spec
%define bindir %{hypre._mpi2_bindir}
%define libdir %{hypre._mpi2_libdir}

%include base.inc
%use hypre = hypre.spec

Name:                   SFEhypre-%{_mpi2_bundle}
IPS_Package_Name:	 	library/math/%{_mpi2_impl}/%{_mpi2_compiler}/hypre
Summary:                High performance preconditioners that features parallel multigrid methods for both structured and unstructured grid problems
Group:                  Utility
Version:                %{hypre.version}
URL:		         	https://computation.llnl.gov/casc/hypre/software.html
License: 		 		LGPLv2.1
Meta(info.upstream): 	HYPRE Support <hypre-support@llnl.gov>
SUNW_Copyright: 		hypre.copyright
SUNW_BaseDir:           %{_basedir}
BuildRoot:              %{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc

BuildRequires: SFE%{_mpi2_bundle}

%description
High performance preconditioners that features parallel multigrid methods for both structured and unstructured grid problems.

NOTE: Built without Python and Java bindings.


%prep
rm -rf %{builddir}
%ifarch amd64 sparcv9
mkdir -p %{builddir}/%_arch64
%hypre_64.prep -d %{builddir}/%_arch64
%endif

mkdir -p %{builddir}/%base_arch
%hypre.prep -d %{builddir}/%base_arch


%build
%ifarch amd64 sparcv9
%hypre_64.build -d %{builddir}/%_arch64
%endif

%hypre.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%hypre_64.install -d %{builddir}/%_arch64
%endif

%hypre.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr (0755, root, bin) %{hypre.libdir}
%{hypre.libdir}/libHYPRE*.so
%ifarch amd64 sparcv9
%dir %attr (0755, root, bin) %{hypre_64.libdir}
%{hypre_64.libdir}/libHYPRE*.so
%endif
%dir %attr (0755, root, bin) %{hypre.includedir}
%{hypre.includedir}/*.h
%dir %attr (0755, root, other) %{hypre.docdir}
%{hypre.docdir}/*.pdf


#------------------------------------------------------------------------------
%changelog
* Wed Nov 21 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 2.9.0.
