#
# spec file for package SFEmpich2
#
# includes module(s): mpich2
#

%include sfe-gcc.inc

%define srcname mpich2

Name:		mpich2-%{_mpi2_compiler}
Version:	1.5
Release:	0
# For release
%define srcversion %{version}
# For following bug-fix releases
#%define srcversion %{version}p%{release}
Source:	http://www.mpich.org/static/tarballs/%{srcversion}/%{srcname}-%{srcversion}.tar.gz
#Source:		http://www.mcs.anl.gov/research/projects/mpich2/downloads/tarballs/%{srcname}-%{srcversion}.tar.gz => URL has changed
#Patch1:		mpich2-01-configure.diff => Fixed since > 1.4.1

# To support multiple MPI implementation let us organize the directories in the following way
# There might be a bug preventing globs to work in %files because %{bld_arch} has quotes
%define mpi2name	%{_mpi2_impl}
%define libdir 		%{_mpi2_libdir}
%define bindir 		%{_mpi2_bindir}
%define sysconfdir 	%{_mpi2_sysconfdir}
%define includedir 	%{_mpi2_includedir}
%define datadir		%{_mpi2_datadir}-%{version}
%define docdir 		%{_mpi2_docdir}-%{version}
%define mandir 		%{_mpi2_mandir}
%define htmldir		%{docdir}/www

%prep
%setup -q -n %{srcname}-%{srcversion}
#%patch1 -p1


%build
CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
if test "x$CPUS" = "x" -o $CPUS = 0; then
    CPUS=1
fi

# We have to make sure mpich2 wrappers pick the compiler used to build mpi librairies
export CFLAGS="%{optflags} "
export CXXFLAGS="%cxx_optflags "
export FFLAGS="%{optflags} "
export FCFLAGS="%{optflags} "
export LDFLAGS="%_ldflags "

%if %{cc_is_gcc}
export CC=%{_gcc_cc}
export CXX=%{_gcc_cxx}
export F77=%{_gcc_f77}
export FC=%{_gcc_fc}
export CFLAGS="$CFLAGS -Wa,--divide "
%endif

./configure --prefix=%{_prefix} \
	--bindir=%{bindir} \
	--libdir=%{libdir} \
	--includedir=%{includedir} \
	--datadir=%{datadir} \
	--docdir=%{docdir} \
	--htmldir=%{htmldir} \
	--mandir=%{mandir} \
	--sysconfdir=%{sysconfdir} \
	--enable-shared --enable-rpath \
    --enable-f77 --enable-fc --enable-cxx --enable-mpe \
	--with-thread-package=posix

make -j $CPUS


%install
make DESTDIR=$RPM_BUILD_ROOT install

# bug in Makefiles
%if %{opt_arch64}
mkdir $RPM_BUILD_ROOT%{htmldir}
cp -pr $RPM_BUILD_ROOT%{datadir}/doc/* \
	$RPM_BUILD_ROOT%{htmldir}
%endif 

# move pkgconfig files
mv $RPM_BUILD_ROOT%{libdir}/pkgconfig $RPM_BUILD_ROOT%{_libdir}

# cleaning unnecessary files
rm -rf $RPM_BUILD_ROOT%{datadir}/doc
rm -rf $RPM_BUILD_ROOT%{datadir}/logfiles
rm -f $RPM_BUILD_ROOT%{libdir}/*.jar
rm -f $RPM_BUILD_ROOT%{libdir}/*.o
rm -rf $RPM_BUILD_ROOT%{libdir}/trace_rlog
rm -rf $RPM_BUILD_ROOT%{_prefix}/sbin
rm -f $RPM_BUILD_ROOT%{bindir}/clog2_join
rm -f $RPM_BUILD_ROOT%{bindir}/clog2_print
rm -f $RPM_BUILD_ROOT%{bindir}/clog2_repair
rm -f $RPM_BUILD_ROOT%{bindir}/clogprint
rm -f $RPM_BUILD_ROOT%{bindir}/clog2print 
rm -f $RPM_BUILD_ROOT%{bindir}/clogTOslog2
rm -f $RPM_BUILD_ROOT%{bindir}/clog2TOslog2 
rm -f $RPM_BUILD_ROOT%{bindir}/jumpshot
rm -f $RPM_BUILD_ROOT%{bindir}/logconvertor
rm -f $RPM_BUILD_ROOT%{bindir}/rlogTOslog2
rm -f $RPM_BUILD_ROOT%{bindir}/rlog_check_timeorder
rm -f $RPM_BUILD_ROOT%{bindir}/rlog_print
rm -f $RPM_BUILD_ROOT%{bindir}/rlogprint
rm -f $RPM_BUILD_ROOT%{bindir}/slog2filter
rm -f $RPM_BUILD_ROOT%{bindir}/slog2navigator
rm -f $RPM_BUILD_ROOT%{bindir}/slog2print
rm -f $RPM_BUILD_ROOT%{bindir}/slog2updater

%if %{cc_is_gcc}
# set absolute path to gcc4 to avoid conflict with gcc3
sed -i -e 's|-Wa,--divide||' \
    $RPM_BUILD_ROOT%{bindir}/mpicc
sed -i -e 's|CC="gcc"|CC="%{_gcc_cc}"|' \
    $RPM_BUILD_ROOT%{bindir}/mpicc
sed -i -e 's|CXX="g++"|CXX="%{_gcc_cxx}"|' \
    $RPM_BUILD_ROOT%{bindir}/mpicxx
sed -i -e 's|F77="gfortran"|F77="%{%{_gcc_f77}"|' \
    $RPM_BUILD_ROOT%{bindir}/mpif77
sed -i -e 's|FC="gfortran"|FC="%{_gcc_fc}"|' \
    $RPM_BUILD_ROOT%{bindir}/mpif90
%endif

# set the base directory as prefix instead of buildroot/prefix in wrappers, configuration and documentation Makefiles
for f in `ggrep -rl "$RPM_BUILD_ROOT" $RPM_BUILD_ROOT`; do
    sed -i -e 's|$RPM_BUILD_ROOT||g' $f;
done;

# need to move the documentation to avoid collision with other mpi implementations
for i in 1 3;
do
	if test ! -d $RPM_BUILD_ROOT%{mandir}/man${i}%{mpi2name};
	then
		mv -u $RPM_BUILD_ROOT%{mandir}/man${i} \
			$RPM_BUILD_ROOT%{mandir}/man${i}%{mpi2name};
	fi;

	rm -rf $RPM_BUILD_ROOT%{mandir}/man${i};
done;


#------------------------------------------------------------------------------
%changelog
* Thu Nov 15 2012 Aurélien Larcher <aurelien.larcher@gmail.com>
- Bump to version 1.5 and added support for 32/64 builds
