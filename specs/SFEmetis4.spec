#
# spec file for package SFEmetis4
#
# includes module(s): metis4
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include Solaris.inc
# SunCC

%ifarch amd64 sparcv9
%include arch64.inc
%use metis4_64 = metis4.spec
%endif

%include base.inc
%use metis4 = metis4.spec

Name:                   SFEmetis4
IPS_Package_Name:	 	library/math/metis4
Summary:                Serial Graph Partitioning and Fill-reducing Matrix Ordering
Group:                  Utility
Version:                %{metis4.version}
URL:		         	http://glaros.dtc.umn.edu/
License: 		 		University of Minnesota METIS License
SUNW_Copyright: 		%{metis4.name}.copyright
SUNW_BaseDir:           %{_basedir}
BuildRoot:              %{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc


%description
METIS is a set of serial programs for partitioning graphs, partitioning finite element meshes, and producing fill reducing orderings for sparse matrices. The algorithms implemented in METIS are based on the multilevel recursive-bisection, multilevel k-way, and multi-constraint partitioning schemes developed in our lab.


%prep
rm -rf %{builddir}
%ifarch amd64 sparcv9
mkdir -p %{builddir}/%_arch64
%metis4_64.prep -d %{builddir}/%_arch64
%endif

mkdir -p %{builddir}/%base_arch
%metis4.prep -d %{builddir}/%base_arch


%build
%ifarch amd64 sparcv9
%metis4_64.build -d %{builddir}/%_arch64
%endif

%metis4.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%metis4_64.install -d %{builddir}/%_arch64
%endif

%metis4.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr (0755, root, bin) %{metis4.bindir}
%{metis4.bindir}/graphchk
%{metis4.bindir}/kmetis
%{metis4.bindir}/mesh2dual
%{metis4.bindir}/mesh2nodal
%{metis4.bindir}/oemetis
%{metis4.bindir}/onmetis
%{metis4.bindir}/partdmesh
%{metis4.bindir}/partnmesh
%{metis4.bindir}/pmetis
%dir %attr (0755, root, bin) %{metis4.libdir}
%{metis4.libdir}/libmetis.a
%ifarch amd64 sparcv9
%dir %attr (0755, root, bin)  %{_bindir}/%{_arch64}
%{_bindir}/%{_arch64}/graphchk
%{_bindir}/%{_arch64}/kmetis
%{_bindir}/%{_arch64}/mesh2dual
%{_bindir}/%{_arch64}/mesh2nodal
%{_bindir}/%{_arch64}/oemetis
%{_bindir}/%{_arch64}/onmetis
%{_bindir}/%{_arch64}/partdmesh
%{_bindir}/%{_arch64}/partnmesh
%{_bindir}/%{_arch64}/pmetis
%dir %attr (0755, root, bin) %{_libdir}/%{_arch64}/%{metis4_64.name}
%{_libdir}/%{_arch64}/%{metis4_64.name}/libmetis.a
%endif
%dir %attr (0755, root, bin) %{metis4.includedir}
%{metis4.includedir}/*.h
%dir %attr (0755, root, bin) %{metis4.docdir}
%{metis4_64.docdir}/manual.ps



#------------------------------------------------------------------------------
%changelog
* Sun Nov 25 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 4.0.3 with no shared lib.
