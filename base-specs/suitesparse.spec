#
# spec file for package SFEsuitesparse
#
# includes module(s): suitesparse
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include sfe-gcc.inc

%define srcname SuiteSparse

Name:					suitesparse
Version:                4.0.2
Source:		         	http://www.cise.ufl.edu/research/sparse/%{srcname}/%{srcname}-%{version}.tar.gz

%define libdir			%{_libdir}/%{name}
%define includedir		%{_includedir}/%{name}

%prep
%setup -q -n %{srcname}

sed -i -e 's|/usr/local/lib|${RPM_BUILD_ROOT}%{libdir}|g' SuiteSparse_config/SuiteSparse_config.mk
sed -i -e 's|/usr/local/include|${RPM_BUILD_ROOT}%{includedir}|g' SuiteSparse_config/SuiteSparse_config.mk
sed -i -e 's|\-lblas|\-lf77blas \-lcblas \-latlas|g' SuiteSparse_config/SuiteSparse_config.mk

%build
#CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
#if test "x$CPUS" = "x" -o $CPUS = 0; then
#    CPUS=1
#fi

%if %{cc_is_gcc}
#
export CC=%{_gcc_cc}
export F77=%{_gcc_f77}
export FC=%{_gcc_fc}
%endif



%define blaslibdir %{_libdir}/atlas
%define blasincludedir %{_includedir}/atlas

export CFLAGS="%optflags -I -L%{blaslibdir} -R%{blaslibdir}"
export FFLAGS="%optflags -L%{blaslibdir} -R%{blaslibdir}"
export FCLAGS="%optflags -L%{blaslibdir} -R%{blaslibdir}"
export LDFLAGS="%_ldflags -L%{blaslibdir} -R%{blaslibdir}"

%if %{opt_arch64}
%define 64bit_flag -DBLAS64
%endif

#make -j $CPUS
make


%install

mkdir -p ${RPM_BUILD_ROOT}%{libdir}
mkdir -p ${RPM_BUILD_ROOT}%{includedir}

make DESTDIR=$RPM_BUILD_ROOT install


%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Wed Nov 21 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 5.6.1.
