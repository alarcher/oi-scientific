#
# Copyright (c) 2006 Sun Microsystems, Inc.
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include Solaris.inc
%include usr-gnu.inc
%define srcname libtool

Name:                SFElibtool
IPS_Package_Name:	 developer/build/libtool
Summary:             Generic library support script
Version:             2.2.10
Source:              http://ftp.gnu.org/gnu/%{srcname}/%{srcname}-%{version}.tar.gz
SUNW_BaseDir:        %{_basedir}
SUNW_Copyright:      libtool.copyright
BuildRoot:           %{_tmppath}/%{name}-%{version}-build
%include default-depend.inc

Requires: SUNWbash
Requires: SUNWpostrun

%define infodir /usr/gnu/share/info

%description
GNU libtool is a generic library support script. Libtool hides the complexity of using shared libraries behind a consistent, portable interface. 

%prep
%setup -q -n %{srcname}-%{version}

%build
CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
if test "x$CPUS" = "x" -o $CPUS = 0; then
     CPUS=1
fi

export CFLAGS="%optflags"
export LDFLAGS="%_ldflags"

./configure \
    --prefix=%{_prefix} \
    --infodir=%{infodir}

make -j$CPUS

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
rm $RPM_BUILD_ROOT%{_libdir}/libltdl.a  
rm $RPM_BUILD_ROOT%{_libdir}/libltdl.la
rm $RPM_BUILD_ROOT%{_datadir}/info/dir

%clean
rm -rf $RPM_BUILD_ROOT

#%post#
#( echo 'PATH=/usr/bin:/usr/sfw/bin; export PATH' ;
#  echo 'infos="';
#  echo 'libtool.info' ;
#  echo '"';
#  echo 'retval=0';
#  echo 'for info in $infos; do';
#  echo '  install-info --info-dir=%{infodir} %{infodir}/$info || retval=1';
#  echo 'done';
#  echo 'exit $retval' ) | $PKG_INSTALL_ROOT/usr/lib/postrun -b -c SFE

#%preun
#( echo 'PATH=/usr/bin:/usr/sfw/bin; export PATH' ;
#  echo 'infos="';
#  echo 'libtool.info' ;
#  echo '"';
#  echo 'for info in $infos; do';
#  echo '  install-info --info-dir=%{infodir} --delete %{infodir}/$info';
#  echo 'done';
#  echo 'exit 0' ) | $PKG_INSTALL_ROOT/usr/lib/postrun -b -c SFE

%files
%defattr (-, root, bin)
%dir %attr (0755, root, bin) %{_bindir}
%{_bindir}/*
%dir %attr (0755, root, bin) %{_libdir}
%{_libdir}/lib*.so*
%dir %attr (0755, root, bin) %{_includedir}
%{_includedir}/*.h
%dir %attr (0755, root, bin) %{_includedir}/libltdl
%{_includedir}/libltdl/*.h
%dir %attr (0755, root, sys) %{_datadir}
%dir %attr (0755, root, bin) %{_datadir}/info
%{_datadir}/info/libtool.info
%{_datadir}/info/libtool.info-1
%{_datadir}/info/libtool.info-2
%dir %attr (0755, root, bin) %{_datadir}/man
%{_datadir}/man/man1/libtool.1
%{_datadir}/man/man1/libtoolize.1
# defined as (0755, root, other) in pkg://localhost/sfe/developer/build/cmake
%dir %attr (0755, root, bin) %{_datadir}/aclocal
%{_datadir}/aclocal/*
%dir %attr (0755, root, other) %{_datadir}/libtool
%{_datadir}/libtool/*

%changelog
* Wed May 26 2010 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Bump to 2.2.10.
* Wed May 26 2010 - Brian Cameron <brian.cameron@oracle.com>
- Bump to 2.2.6b.
* Sat May 24 2008 - Mark Wright <markwright@internode.on.net>
- Bump to 2.2.4.  Add patch1 to use bash.
* Sun Mar 2 2008 - Mark Wright <markwright@internode.on.net>
- Bump to 1.5.26.
* Thu Mar 22 2007 - nonsea@users.sourceforge.net
- Bump to 1.5.24
- Use http url in Source.
* Thu Mar 22 2007 - nonsea@users.sourceforge.net
- Add Requires/BuildRequries after check-deps.pl run.
* Mon Jan 15 2007 - daymobrew@users.sourceforge.net
- Add SUNWtexi dependency.
* Sun Jan  7 2007 - laca@sun.com
- fix infodir permissions, update info dir file using postrun scripts
* Wed Dec 20 2006 - Eric Boutilier
- Initial spec
