#
# spec file for package SFEsinglearch
#
# includes module(s): singlearch
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include Solaris.inc
%define cc_is_gcc 1

%include base.inc
%use singlearch = singlearch.spec

Name:					SFEsinglearch
IPS_Package_Name:		library/singlearch
Version:                __add_version__
Source:					http://singlearch.example/%{srcname}-%{version}.tar.gz
Summary:				__add_summary__
Group:					Utility
URL:					__add_url__
License:				__add_license__
SUNW_Copyright:			%{singlearch.name}.copyright
SUNW_BaseDir:			%{_basedir}
BuildRoot:				%{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc

BuildRequires:	SFEgcc-46
Requires:  		SFEgcc-runtime

%description
__add_description__


%prep
%setup -q -n %{srcname}-%{version}


%build
CPUS=`/usr/sbin/psrinfo | grep on-line | wc -l | tr -d ' '`
if test "x$CPUS" = "x" -o $CPUS = 0; then
    CPUS=1
fi

export CC=gcc
export CXX=g++
export F77=gfortran
export FC=gfortran 

export CFLAGS="%optflags "
export CXXFLAGS="%cxx_optflags "
export FFLAGS="%optflags "
export FCLAGS="%optflags "
export LDFLAGS="%_ldflags "


./configure --prefix=%{_prefix} \
			--bindir=%{_bindir} \
			--libdir=%{_libdir} \
			--includedir=%{_includedir}/%{name}

make -j $CPUS


%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install


%clean
rm -rf $RPM_BUILD_ROOT


%changelog
* Mon Nov 19 2012 - My name <maintainer@example.com>
- Intial spec for a bogus package :)
