#
# spec file for package SFEgts
#
# includes module(s): gts
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.

%include Solaris.inc
#%define %{cc_is_gcc}	1

%ifarch amd64 sparcv9
%include arch64.inc
%use gts_64 = gts.spec
%endif

%include base.inc
%use gts = gts.spec

Name:					SFEgts
IPS_Package_Name:		library/gts
Summary:				GNU Triangulated Surface Library
Group:					Utility
Version:				%{gts.version}
URL:					http://gts.sourceforge.net/
License:				GNU GPLv2
SUNW_Copyright:			%{gts.name}.copyright
SUNW_BaseDir:			%{_basedir}
BuildRoot:				%{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc

%if %{cc_is_gcc}
BuildRequires:	SFEgcc-46
Requires:  		SFEgcc-runtime
%endif

%description
GTS stands for the GNU Triangulated Surface Library. It is an Open Source Free Software Library intended to provide a set of useful functions to deal with 3D surfaces meshed with interconnected triangles. The source code is available free of charge under the Free Software LGPL license.

The code is written entirely in C with an object-oriented approach based mostly on the design of GTK+. Careful attention is paid to performance related issues as the initial goal of GTS is to provide a simple and efficient library to scientists dealing with 3D computational surface meshes.

A brief summary of its main features:

-    Simple object-oriented structure giving easy access to topological properties.
-    2D dynamic Delaunay and constrained Delaunay triangulations.
-    Robust geometric predicates (orientation, in circle) using fast adaptive floating point arithmetic (adapted from the fine work of Jonathan R. Shewchuk).
-    Robust set operations on surfaces (union, intersection, difference).
-    Surface refinement and coarsening (multiresolution models).
-    Dynamic view-independent continuous level-of-detail.
-    Preliminary support for view-dependent level-of-detail.
-    Bounding-boxes trees and Kd-trees for efficient point location and collision/intersection detection.
-    Graph operations: traversal, graph partitioning.
-    Metric operations (area, volume, curvature ...).
-    Triangle strips generation for fast rendering.


%prep
rm -rf %{builddir}
%ifarch amd64 sparcv9
mkdir -p %{builddir}/%_arch64
%gts_64.prep -d %{builddir}/%_arch64
%endif

mkdir -p %{builddir}/%base_arch
%gts.prep -d %{builddir}/%base_arch


%build
%ifarch amd64 sparcv9
%gts_64.build -d %{builddir}/%_arch64
%endif

%gts.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%gts_64.install -d %{builddir}/%_arch64
%endif

%gts.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr (0755, root, bin) %{_bindir}
%{_bindir}/gts2stl
%{_bindir}/gtscheck
%{_bindir}/gtscompare
%{_bindir}/transform
%{_bindir}/gts-config
%{_bindir}/gts2xyz
%{_bindir}/gts2oogl
%{_bindir}/stl2gts
%{_bindir}/gts2dxf
%{_bindir}/delaunay
%{_bindir}/gtstemplate
%dir %attr (0755, root, bin) %{_libdir}
%{_libdir}/libgts-0.7.so.5.0.1
%{_libdir}/libgts-0.7.so.5
%{_libdir}/libgts.so
%{_libdir}/libgts.la
%{_libdir}/libgts.a
%dir %attr (0755, root, other) %{_libdir}/pkgconfig
%{_libdir}/pkgconfig/gts.pc
%ifarch amd64 sparcv9
%dir %attr (0755, root, bin) %{_bindir}/%{_arch64}
%{_bindir}/%{_arch64}/gts2stl
%{_bindir}/%{_arch64}/gtscheck
%{_bindir}/%{_arch64}/gtscompare
%{_bindir}/%{_arch64}/transform
%{_bindir}/%{_arch64}/gts-config
%{_bindir}/%{_arch64}/gts2xyz
%{_bindir}/%{_arch64}/gts2oogl
%{_bindir}/%{_arch64}/stl2gts
%{_bindir}/%{_arch64}/gts2dxf
%{_bindir}/%{_arch64}/delaunay
%{_bindir}/%{_arch64}/gtstemplate
%dir %attr (0755, root, bin) %{_libdir}/%{_arch64}
%{_libdir}/%{_arch64}/libgts-0.7.so.5.0.1
%{_libdir}/%{_arch64}/libgts-0.7.so.5
%{_libdir}/%{_arch64}/libgts.so
%{_libdir}/%{_arch64}/libgts.la
%{_libdir}/%{_arch64}/libgts.a
%dir %attr (0755, root, other) %{_libdir}/%{_arch64}/pkgconfig
%{_libdir}/%{_arch64}/pkgconfig/gts.pc
%endif
%dir %attr (0755, root, bin) %{_includedir}
%{_includedir}/gts/gts.h
%{_includedir}/gts/gtsconfig.h
%dir %attr (0755, root, other) %{_datadir}/aclocal
%{_datadir}/aclocal/gts.m4
%dir %attr (0755, root, bin) %{_datadir}/man
%{_datadir}/man/*


#------------------------------------------------------------------------------
%changelog
* Mon Nov 26 2012 - Aurélien Larcher <aurelien.larcher@gmail.com>
- Intial spec for version 120706.
