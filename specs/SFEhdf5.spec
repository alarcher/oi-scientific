#
# spec file for package SFEhdf5
#
# includes module(s): hdf5
#

%include Solaris.inc
%define cc_is_gcc 1

%ifarch amd64 sparcv9
%include arch64.inc
%use hdf5_64 = hdf5.spec
%endif

%include base.inc
%use hdf5 = hdf5.spec

Name:		SFEhdf5
IPS_package_name: library/hdf5
Summary:	Data model, library, and file format for storing and managing data.
Group:		Utility
Version:	%{hdf5.version}
Release:	%{hdf5.release}
URL:		http://www.hdfgroup.org/HDF5		
License:	NCSA
SUNW_Copyright: %{hdf5.name}.copyright
#Meta(info.upstream):

SUNW_BaseDir: %{_basedir}/%{_subdir}
Docdir:	      %{_defaultdocdir}/doc
BuildRoot:    %{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc

BuildRequires:  SFEgcc-46
Requires:       SFEgccruntime


%description
HDF5 is a data model, library, and file format for storing and managing data. It supports an unlimited variety of datatypes, and is designed for flexible and efficient I/O and for high volume and complex data. HDF5 is portable and is extensible, allowing applications to evolve in their use of HDF5. The HDF5 Technology suite includes tools and applications for managing, manipulating, viewing, and analyzing data in the HDF5 format.


%package devel
Summary:       %{summary} - development files
SUNW_BaseDir:  %{_basedir}/%{_subdir}
Requires:      %name


%prep
rm -rf %name-%version
%ifarch amd64 sparcv9
mkdir -p %name-%version/%_arch64
%hdf5_64.prep -d %name-%version/%_arch64
%endif

mkdir -p %name-%version/%base_arch
%hdf5.prep -d %name-%version/%base_arch


%build
%ifarch amd64 sparcv9
%hdf5_64.build -d %name-%version/%_arch64
%endif

%hdf5.build -d %name-%version/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%hdf5_64.install -d %name-%version/%_arch64
%endif

%hdf5.install -d %name-%version/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr(0755,root,bin) %{_bindir}
%{_bindir}/*h5*
%dir %attr (0755, root, bin) %{_libdir}
%{_libdir}/lib*.so*
%{_libdir}/lib*.a
%{_libdir}/lib*.la
%{_libdir}/libhdf5.settings
%ifarch amd64 sparcv9
%dir %attr(0755,root,bin) %{_bindir}/%_arch64
%{_bindir}/%_arch64/*h5*
%dir %attr (0755, root, bin) %{_libdir}/%_arch64
%{_libdir}/%_arch64/lib*.so*
%{_libdir}/%_arch64/lib*.a
%{_libdir}/%_arch64/lib*.la
%{_libdir}/%_arch64/libhdf5.settings
%endif
%dir %attr(0755,root,bin) %{_datadir}/%{hdf5.name}-%{version}
%{_datadir}/%{hdf5.name}-%{version}/examples/*


%files devel
%dir %attr (0755, root, bin) %{_includedir}
%dir %attr (0755, root, other) %{_includedir}/%{hdf5.name}
%{_includedir}/%{hdf5.name}/*.h
%{_includedir}/%{hdf5.name}/*.mod


#------------------------------------------------------------------------------
%changelog
* Mon Nov 19 2012 Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial version 1.8.10 with only serial support for 32/64 builds
