#
# spec file for package SFEjogl2
#
# includes module(s): jogl2
#

%include Solaris.inc

%ifarch amd64 sparcv9
%include arch64.inc
%use jogl2_64 = jogl2.spec
%endif

%include base.inc
%use jogl2 = jogl2.spec

Name:		SFEjogl2
IPS_package_name: library/java/jogl2
Summary:	Java Binding for the OpenGL® AP
Group:		Development/Languages/Java
Version:	%{jogl2.version}
Release:	%{jogl2.release}
URL:		http://jogamp.org/jogl/www
License:	BSD 2-clause, BSD 3-clause, BSD 4-clause
SUNW_Copyright: %{jogl2.name}.copyright
Meta(info.upstream): JOGAMP forum <http://forum.jogamp.org/>
SUNW_BaseDir: %{_basedir}/%{_subdir}
Docdir:	      %{_defaultdocdir}/doc
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
%define builddir		%{name}-%{version}
%include default-depend.inc


%description
JOGL provides full access to the APIs in the OpenGL 1.3 - 3.0, 3.1 - 3.3, >= 4.0, ES 1.x and ES 2.x specification as well as nearly all vendor extensions. OpenGL Evolution & JOGL (UML) gives you a brief overview of OpenGL, its profiles and how we map them to JOGL.
JOGL integrates with the AWT, Swing and SWT widget sets, as well with custom windowing toolkits using the NativeWindow API. JOGL also provides its own native windowing toolkit, NEWT. 

%prep
rm -rf %{builddir}
%ifarch amd64 sparcv9
mkdir -p %{builddir}/%_arch64
%jogl2_64.prep -d %{builddir}/%_arch64
%endif

mkdir -p %{builddir}/%base_arch
%jogl2.prep -d %{builddir}/%base_arch


%build
%ifarch amd64 sparcv9
%jogl2_64.build -d %{builddir}/%_arch64
%endif

%jogl2.build -d %{builddir}/%{base_arch}


%install
rm -rf %{buildroot}
%ifarch amd64 sparcv9
%jogl2_64.install -d %{builddir}/%_arch64
%endif

%jogl2.install -d %{builddir}/%{base_arch}


%clean
rm -rf %{buildroot}


%files
%defattr (-, root, bin)
%dir %attr (0755, root, bin) %{jogl2.libdir}
%{jogl2.libdir}/jogl.jar
%{jogl2.libdir}/jogl-rt.jar
%{jogl2.libdir}/jogl-rt-natives-solaris-amd64.jar
%{jogl2.libdir}/libjogl-rt.so
%ifarch amd64 sparcv9
%dir %attr (0755, root, bin) %{_libdir}/%_arch64/%{jogl2_64.name}
%{_libdir}/%_arch64/%{jogl2_64.name}/jogl.jar
%{_libdir}/%_arch64/%{jogl2_64.name}/jogl-rt.jar
%{_libdir}/%_arch64/%{jogl2_64.name}/jogl-rt-natives-solaris-amd64.jar
%{_libdir}/%_arch64/%{jogl2_64.name}/libjogl-rt.so
%endif
%dir %attr (0755, root, sys) %{jogl2.datadir}
%{jogl2.datadir}/artifact.properties
%{jogl2.datadir}/jogl-java-src.zip
%{jogl2.datadir}/jogl-rt.jnlp
%dir %attr (0755, root, sys) %{jogl2.datadir}/src
%{jogl2.datadir}/src/*


#------------------------------------------------------------------------------
%changelog
* Tue Nov 21 2012 Aurélien Larcher <aurelien.larcher@gmail.com>
- Initial spec for version 2.0-rc11 
